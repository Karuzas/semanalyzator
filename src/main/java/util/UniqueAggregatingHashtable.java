package util;

import java.util.List;


public class UniqueAggregatingHashtable extends AggregatingHashtable {

    public UniqueAggregatingHashtable() {
        super();
    }

    protected boolean shouldAdd(List list, Object value) {
        return list == null || list.indexOf(value) < 0;
    }

}
