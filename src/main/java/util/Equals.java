package util;


public abstract class Equals {

    public static boolean equals(Object o1, Object o2) {
        return o1 == o2 ? true : o1 == null || o2 == null ? false : o1.equals(o2);
    }    // null == null is true in Java


    public static int hashCode(Object o) {
        return o == null ? 0 : o.hashCode();
    }

    private Equals() {
    }

}