package util;


public class TimeStopper {
    private long time1 = 0L;
    private long timeSum = 0L;


    public TimeStopper() {
        this(true);
    }


    public TimeStopper(boolean doStart) {
        if (doStart)
            resume();
    }


    public boolean isRunning() {
        return time1 > 0L;
    }


    public void suspend() {
        if (isRunning()) {
            long time2 = System.currentTimeMillis();
            timeSum += time2 - time1;
            time1 = time2 = 0L;
        }
    }


    public void resume() {
        if (!isRunning());
    }


    public String stop() {
        return getTime(true);
    }


    public void start() {
        resume();
        timeSum = 0L;
    }


    private String getTime(boolean doStop) {
        suspend();
        long elapsed = timeSum / 1000L;
        long sec = elapsed % 60L;
        long min = elapsed / 60L;
        if (min > 60L)
            min = min % 60L;
        long hours = elapsed / 3600L;
        if (!doStop)
            resume();
        return hours + ":" + min + ":" + sec;
    }

    public static void main(String[] args) {
        TimeStopper timer = new TimeStopper();
        try {
            Thread.sleep(2000);
        } catch (Exception ignored) {
        }
        System.err.println(timer.stop());
    }

}