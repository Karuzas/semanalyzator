package util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.*;


public class AggregatingHashtable extends Hashtable {
    public AggregatingHashtable() {
        super();
    }

    public AggregatingHashtable(int initialCapacity) {
        super(initialCapacity);
    }


    public Object put(Object key, Object value) {
        List list = (List) super.get(key);
        Object ret = null;

        if (list == null) {
            list = createAggregationList();
            super.put(key, list);
        } else {
            if (shouldAdd(list, value) == false)
                return list;

            ret = list;
        }

        list.add(value);
        return ret;
    }


    protected boolean shouldAdd(List list, Object value) {
        return true;
    }


    protected List createAggregationList() {
        return new ArrayList();
    }


    public void replace(Object key, List newList) {
        super.put(key, newList);
    }


    // Must adjust deserialization as put() will put value lists into newly provided lists
    private void readObject(ObjectInputStream s)
            throws IOException, ClassNotFoundException {
        s.defaultReadObject();

        for (Iterator it = entrySet().iterator(); it.hasNext(); ) {
            Map.Entry entry = (Map.Entry) it.next();
            List list = (List) entry.getValue();
            Object o = list.size() == 1 ? list.get(0) : null;    // resolve the list that has been put by super.readObject()
            if (o instanceof List) {    // apache commons MultiHashMap came around with that problem only on JDK 1.2 and 1.3 ?
                super.put(entry.getKey(), o);
            }
        }
    }

}
