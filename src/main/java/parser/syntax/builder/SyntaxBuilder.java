package parser.syntax.builder;

import parser.Lexer;
import parser.Parser;
import parser.ParserTables;
import parser.lexer.LexerBuilder;
import parser.lexer.LexerException;
import parser.lexer.StandardLexerRules;
import parser.parsertables.ParserBuildException;
import parser.syntax.Syntax;
import parser.syntax.SyntaxException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SyntaxBuilder {

    private Syntax syntax;
    private Syntax lexerSyntax;
    private Syntax parserSyntax;
    private Lexer lexer;
    private List tokenSymbols;
    private List ignoredSymbols;

    public SyntaxBuilder(Object syntaxInput) throws SyntaxException, LexerException, ParserBuildException, IOException {

        // build the hardcoded default BNF lexer
        SyntaxSeparation.DEBUG = false;
        SyntaxSeparation separation = new SyntaxSeparation(new Syntax(StandardLexerRules.lexerSyntax));
        SyntaxSeparation.DEBUG = true;

        LexerBuilder builder = new LexerBuilder(separation.getLexerSyntax(), separation.getIgnoredSymbols());
        Lexer lexer = builder.getLexer();
        lexer.setInput(syntaxInput);

        ParserTables parserTables = new SyntaxBuilderParserTables();

        // start the BNF parser with syntax input
        Parser parser = new Parser(parserTables);
        List initialNonTerminals = new ArrayList(64);
        boolean ok = parser.parse(lexer, new SyntaxBuilderSemantic(initialNonTerminals));
        if (!ok)
            throw new SyntaxException("Failed building Syntax from " + syntaxInput);

        List result = (List) parser.getResult();    // must be a List, according to applied semantic
        List rules = new ArrayList();    // can not predict size
        ArtificialRule.resolveArtificialRules(result, rules);
        this.syntax = new Syntax(rules);
        System.err.println("Built result syntax:\n" + this.syntax);
    }

    private void ensureSeparation() throws SyntaxException {
        if (tokenSymbols == null) {
            SyntaxSeparation separation = new SyntaxSeparation(syntax);
            this.tokenSymbols = separation.getTokenSymbols();
            this.ignoredSymbols = separation.getIgnoredSymbols();
            this.parserSyntax = separation.getParserSyntax();
            this.lexerSyntax = separation.getLexerSyntax();
        }
    }


    public Lexer getLexer() throws LexerException, SyntaxException {
        if (lexer == null) {
            ensureSeparation();
            LexerBuilder builder = new LexerBuilder(lexerSyntax, ignoredSymbols);
            this.lexer = builder.getLexer();
        }
        return this.lexer;
    }


    public Syntax getParserSyntax() throws SyntaxException {
        ensureSeparation();
        return this.parserSyntax;
    }


    public Syntax getSyntax() {
        return syntax;
    }

}
