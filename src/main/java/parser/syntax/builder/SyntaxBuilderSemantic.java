package parser.syntax.builder;

import parser.Token;
import semantic.Semantic;
import parser.syntax.Rule;

import java.util.ArrayList;
import java.util.List;


public class SyntaxBuilderSemantic implements Semantic {


    private List initialNonterminals;


    public SyntaxBuilderSemantic() {
        this(null);
    }


    public SyntaxBuilderSemantic(List initialNonterminals) {
        this.initialNonterminals = initialNonterminals;
    }


    public Object doSemantic(Rule rule, List<Object> inputTokens, List<Token.Range> ranges) {
        String nt = rule.getNonterminal();

        if (nt.equals("set"))
            return inputTokens;

        if (nt.equals("intersectionstartunit") || nt.equals("intersectionunit"))
            return inputTokens.get(0);

        if (nt.equals("intersectionsubtract"))
            return inputTokens;

        if (nt.equals("intersectionsubtracts"))
            if (inputTokens.size() == 2)
                return appendAll((List) inputTokens.get(0), (List) inputTokens.get(1));
            else
                return inputTokens.get(0);

        if (nt.equals("intersection"))
            return insertAtStart(inputTokens.get(0), (List) inputTokens.get(1));

        if (nt.equals("sequnit"))
            if (inputTokens.size() == 3)
                return sequnitInParenthesis(inputTokens.get(1));
            else
                return inputTokens.get(0);

        if (nt.equals("quantifiedsequnit"))
            if (inputTokens.size() == 2)
                return quantifiedsequnit(inputTokens.get(0), inputTokens.get(1));
            else
                return inputTokens.get(0);

        if (nt.equals("sequence"))
            if (inputTokens.size() == 2)
                return append((List) inputTokens.get(0), inputTokens.get(1));
            else
                return inputTokens;

        if (nt.equals("sequence_opt"))
            return inputTokens;

        if (nt.equals("unionseq"))
            if (inputTokens.size() == 3)
                return append((List) inputTokens.get(0), inputTokens.get(2));
            else
                return inputTokens;

        if (nt.equals("rule"))
            return rule((String) inputTokens.get(0), (List) inputTokens.get(2));

        if (nt.equals("syntax"))
            if (inputTokens.size() == 2)
                return syntax((List) inputTokens.get(0), (List) inputTokens.get(1));
            else
                return inputTokens.get(0);

        throw new IllegalArgumentException("Unknown rule: " + rule);
    }


    private ArtificialRule sequnitInParenthesis(Object unionseq) {
        return new ArtificialRule((List) unionseq, "OR");
    }

    private ArtificialRule quantifiedsequnit(Object sequnit, Object quantifier) {
        return new ArtificialRule(sequnit, (String) quantifier);
    }

    private List append(List list, Object element) {
        list.add(element);
        return list;
    }

    private List appendAll(List list, List elements) {
        for (int i = 0; i < elements.size(); i++)
            list.add(elements.get(i));
        return list;
    }

    private List insertAtStart(Object intersectionStart, List intersectionList) {
        intersectionList.add(0, intersectionStart);
        return intersectionList;
    }

    private List rule(String identifier, List unionseq) {
        if (initialNonterminals != null && initialNonterminals.indexOf(identifier) < 0)
            initialNonterminals.add(identifier);

        for (int i = 0; i < unionseq.size(); i++) {
            List deep = (List) unionseq.get(i);
            List flat = ArtificialRule.flattenLists(deep, new ArrayList());
            flat.add(0, identifier);
            unionseq.set(i, flat);
        }
        return unionseq;
    }

    private List syntax(List syntax, List rule) {
        for (int i = 0; i < rule.size(); i++)
            syntax.add(rule.get(i));
        return syntax;
    }

}
