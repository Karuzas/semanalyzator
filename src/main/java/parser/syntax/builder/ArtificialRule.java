package parser.syntax.builder;

import parser.Token;

import java.util.ArrayList;
import java.util.List;


class ArtificialRule {
    private List rules;
    private String nonterminal;


    public ArtificialRule(List sentencesInParenthesis, String catSym) {    // make "(b c)" to rule "_b_c_ ::= b c"
        StringBuffer sb = new StringBuffer();
        parenthesisContentsToString(sentencesInParenthesis, sb, catSym);
        nonterminal = ensureUnderscore(sb.toString());
        rules = createRule(nonterminal, sentencesInParenthesis);
    }

    private void parenthesisContentsToString(List sentences, StringBuffer sb, String catSym) {
        for (int i = 0; i < sentences.size(); i++) {
            Object o = sentences.get(i);

            if (o instanceof List) {
                parenthesisContentsToString((List) o, sb, "");    // catenize symbol only on first level
            }


            if (i < sentences.size() - 1)
                sb.append(catSym.length() > 0 ? "_" + catSym : "").append("_");
        }
    }


    public ArtificialRule(Object token, String quantifier) {    // make "a*", "a+", "a?" to rules
        nonterminal = token.toString();

        String listNonterm = null;    // needed for * and +

        if (quantifier.equals("+") || quantifier.equals("*")) {
            List sentences = new ArrayList();

            listNonterm = ensureUnderscore(nonterminal) + "_LIST";

            // for now leave out the left side nonterminal of list
            List sentence = new ArrayList();    // mandatory tokenlist expands to
            sentence.add(listNonterm);    // the list and
            sentence.add(token.toString());    // the token
            sentences.add(sentence);

            sentence = new ArrayList();
            sentence.add(token.toString());    // or the token alone
            sentences.add(sentence);

            rules = createRule(listNonterm, sentences);    // adds the left side nonterminal of list

            if (quantifier.equals("+"))
                nonterminal = listNonterm;    // "_a_LIST" is the substitute for "a+"
        }

        if (quantifier.equals("*") || quantifier.equals("?")) {
            List sentences = new ArrayList();

            nonterminal = ensureUnderscore(nonterminal) + (quantifier.equals("*") ? "_OPTLIST" : "_OPT");

            String nonterm = quantifier.equals("*") ? listNonterm : token.toString();

            List sentence = new ArrayList();    // optional tokenlist expands to
            sentence.add(nonterm);    // the mandatory list when *, the token when ?
            sentences.add(sentence);

            sentences.add(new ArrayList());    // or nothing

            List mandatoryList = quantifier.equals("*") ? rules : null;

            rules = createRule(nonterminal, sentences);

            if (mandatoryList != null)
                rules.addAll(mandatoryList);
        }

        if (token instanceof ArtificialRule)    // could be '(' token ')'
            rules.addAll(((ArtificialRule) token).getRules());
    }


    private String ensureUnderscore(String nonterminal) {
        if (!nonterminal.startsWith(Token.ARTIFICIAL_NONTERMINAL_START_CHARACTER))
            nonterminal = Token.ARTIFICIAL_NONTERMINAL_START_CHARACTER + nonterminal;
        return nonterminal;
    }

    private List createRule(String nonterminal, List sentences) {
        for (int i = 0; i < sentences.size(); i++) {
            List deep = (List) sentences.get(i);
            List flat = flattenLists(deep, new ArrayList());
            flat.add(0, nonterminal);
            sentences.set(i, flat);
        }
        return sentences;
    }


    public static List flattenLists(List deep, List flat) {
        for (Object o : deep) {
            if (o instanceof List)
                flattenLists((List) o, flat);
            else
                flat.add(o);
        }
        return flat;
    }


    public String toString() {
        return nonterminal;
    }


    public List getRules() {
        return rules;
    }


    public static void resolveArtificialRules(List rules, List resultSyntax) {
        for (Object rule1 : rules) {
            List rule = (List) rule1;

            resultSyntax.add(rule);    // add it

            for (int k = 1; k < rule.size(); k++) {    // optionally modify it
                Object symbol = rule.get(k);

                if (symbol instanceof ArtificialRule) {    // when containing rules
                    rule.set(k, symbol.toString());    // set the rule's nonterminal

                    ArtificialRule ar = (ArtificialRule) symbol;
                    resolveArtificialRules(ar.getRules(), resultSyntax);    // add the rule
                }
            }
        }
    }

}