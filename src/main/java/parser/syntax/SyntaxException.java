package parser.syntax;

public class SyntaxException extends Exception {
    public SyntaxException(String msg) {
        super(msg);
    }

}