package parser.syntax;

import java.util.ArrayList;
import java.util.List;


public abstract class SyntaxUtil {

    public static List ruleArrayToList(String[][] array) {
        return appendToSyntax(array, new ArrayList(array.length));
    }


    public static List ruleToList(String[] rule) {
        return appendToRule(rule, new ArrayList(rule.length));
    }


    public static List appendToSyntax(String[][] arrays, List receiver) {
        for (int i = 0; i < arrays.length; i++)
            receiver.add(appendToRule(arrays[i], new ArrayList(arrays[i].length)));
        return receiver;
    }


    public static List appendToRule(String[] array, List receiver) {
        for (int i = 0; i < array.length; i++)
            receiver.add(array[i]);
        return receiver;
    }


    public static List catenizeRules(String[][][] arrays) {
        return catenizeRules(arrays, false);
    }


    public static final List catenizeRulesUnique(String[][][] arrays) {
        return catenizeRules(arrays, true);
    }

    private static final List catenizeRules(String[][][] arrays, boolean checkUnique) {
        int len = 0;
        for (int i = 0; i < arrays.length; i++)    // count global length
            len += arrays[i].length;

        List grammar = new ArrayList(len);
        for (int i = 0; i < arrays.length; i++) {
            for (int j = 0; j < arrays[i].length; j++) {
                List rule = new ArrayList(arrays[i][j].length);
                for (int k = 0; k < arrays[i][j].length; k++)
                    rule.add(arrays[i][j][k]);

                if (checkUnique == false || grammar.indexOf(rule) < 0)
                    grammar.add(rule);
            }
        }
        return grammar;
    }


    public static String maskQuoteAndBackslash(String s) {
        StringBuffer sb = new StringBuffer(s.length());
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            switch (c) {
                case '"':
                case '\\':
                    sb.append('\\');
                    break;
            }
            sb.append(c);
        }
        return sb.toString();
    }


    private SyntaxUtil() {
    }

}