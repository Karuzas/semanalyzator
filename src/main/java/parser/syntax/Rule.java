package parser.syntax;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Rule implements Serializable {
    private List symbols;


    public Rule(String nonterminal, int rightSize) {
        this(new ArrayList(rightSize + 1));
        symbols.add(nonterminal);
    }


    public Rule(String[] symbols) {
        this(SyntaxUtil.ruleToList(symbols));
    }


    public Rule(List symbols) {
        if (symbols == null)
            throw new IllegalArgumentException("Can not construct rule without nonterminal: " + symbols);
        this.symbols = symbols;
    }


    protected Rule() {
    }


    public String getNonterminal() {
        return (String) symbols.get(0);
    }

    public List getSymbols() {
        return symbols;
    }

    public int rightSize() {
        return symbols.size() - 1;
    }

    public String getRightSymbol(int i) {
        return (String) symbols.get(i + 1);
    }

    public void setRightSymbol(String symbol, int i) {
        symbols.set(i + 1, symbol);
    }

    public void addRightSymbol(String symbol) {
        symbols.add(symbol);
    }

    public int indexOnRightSide(String symbol) {
        for (int i = 0; i < rightSize(); i++)
            if (getRightSymbol(i).equals(symbol))
                return i;
        return -1;
    }


    public boolean equals(Object o) {
        return ((Rule) o).symbols.equals(symbols);
    }


    public int hashCode() {
        return symbols.hashCode();
    }


    public String toString() {
        StringBuffer sb = new StringBuffer(getNonterminal() + " ::= ");
        for (int i = 0; i < rightSize(); i++) {
            sb.append(getRightSymbol(i));
            sb.append(" ");
        }
        sb.append(";");
        return sb.toString();
    }

}
