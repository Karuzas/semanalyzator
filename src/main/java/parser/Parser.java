package parser;

import parser.syntax.Rule;
import semantic.Semantic;

import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.*;

public class Parser implements Serializable {
    private Lexer lexer;
    private ParserTables tables;
    private transient Semantic semantic;
    protected Stack<Integer> stateStack = new Stack<Integer>();
    protected Stack<Object> valueStack = new Stack<Object>();
    protected Stack<Token.Range> rangeStack = new Stack<Token.Range>();
    private transient Object result;
    private transient List<Object> inputTokens;
    private transient List<Token.Range> rangeList;
    private transient Token.Range range = new Token.Range(null, null);
    private transient PrintStream out;
    private boolean passExpectedToLexer = true;
    private boolean DEBUG;

    public Parser(ParserTables tables) {
        this.tables = tables;
    }

    public Object getResult() {
        return result;
    }

    public void setLexer(Lexer lexer) {
        boolean initLexer = (this.lexer != lexer);    // look if passed lexer needs terminals
        this.lexer = lexer;
        clear();    // clear if reused
        if (initLexer)
            lexer.setTerminals(getParserTables().getTerminals());    // pass terminals to lexer
    }

    public Lexer getLexer() {
        return lexer;
    }

    public void setInput(Object input)
            throws IOException {
        if (lexer == null)
            throw new IllegalStateException("Can not set input when no lexer was defined!");
        clear();    // clear if reused
        lexer.setInput(input);
    }

    public void setSemantic(Semantic semantic) {
        this.semantic = semantic;
    }

    public Semantic getSemantic() {
        return semantic;
    }

    public ParserTables getParserTables() {
        return tables;
    }

    public void setPassExpectedToLexer(boolean passExpectedToLexer) {
        this.passExpectedToLexer = passExpectedToLexer;
    }

    private Integer top() {
        return stateStack.peek();
    }

    private void push(Integer state, Object result, Token.Range range) {
        stateStack.push(state);
        semanticPush(result, range);
    }

    private void pop(int pops) {
        inputTokens = new ArrayList<Object>();
        rangeList = new ArrayList<Token.Range>();

        for (int i = 0; i < pops; i++) {
            stateStack.pop();
            semanticPop(i, pops);
        }
    }

    private void semanticPush(Object result, Token.Range range) {
        if (semantic != null) {
            valueStack.push(result);    // we need to know parse result
            rangeStack.push(range);    // and its start-end positions within input text
        }
    }

    private void semanticPop(int popIndex, int countOfPops) {
        if (semantic != null) {
            // the value pop
            inputTokens.add(0, valueStack.pop());

            // the range pop
            Token.Range currentRange = rangeStack.pop();
            rangeList.add(0, currentRange);

            if (popIndex == 0)    // first pop of right side holds last token value
                this.range = new Token.Range(null, currentRange.end);    // helper to remember end address

            if (popIndex == countOfPops - 1)    // if it is the last pop, make a valid range for next push()
                this.range = new Token.Range(currentRange.start, this.range.end);
        }
    }

    protected void reduce(Integer ruleIndex) {
        if (DEBUG)
            dump("reduce " + ruleIndex);

        Rule rule = getParserTables().getSyntax().getRule(ruleIndex);
        pop(rule.rightSize());    // pop count of elements on right side

        semanticReduce(rule);

        String nonterminal = rule.getNonterminal();
        push(getParserTables().getGotoState(top(), nonterminal), result, range);

        dumpStack();
    }

    private void semanticReduce(Rule rule) {
        if (semantic != null) {
            result = semantic.doSemantic(rule, inputTokens, rangeList);
        }
    }

    protected Token shift(Token token)
            throws IOException {
        if (DEBUG)
            dump("shift from token symbol >" + token.symbol + "<");

        push(getParserTables().getGotoState(top(), token.symbol), token.text, token.range);
        dumpStack();

        Token newToken = getNextToken();
        if (DEBUG)
            dump("next token " + newToken.symbol + " >" + newToken.text + "<");

        return newToken;
    }

    protected Token getNextToken()
            throws IOException {
        Map expected = passExpectedToLexer && top() >= 0 ? getParserTables().getExpected(top()) : null;
        Token token = lexer.getNextToken(expected);
        return token;
    }


    // public parsing methods

    public boolean parse(Lexer lexer, Semantic semantic)
            throws IOException {
        setLexer(lexer);
        setSemantic(semantic);
        return parse();
    }

    public boolean parse() throws IOException {
        stateStack.push(0);    // push first state on stack
        Integer action = ParserTables.SHIFT;    // some allowed initial value
        Token token = getNextToken();    // start reading input
        if (DEBUG)
            dump("initial token symbol >" + token.symbol + "<, text >" + token.text + "<");

        while (token.symbol != null &&    // lexer error
                !action.equals(ParserTables.ACCEPT) &&    // input accepted
                !action.equals(ParserTables.ERROR) &&    // parse-action table error
                !top().equals(ParserTables.ERROR))    // goto table error
        {
            action = getParserTables().getParseAction(top(), token.symbol);

            if (action > 0)
                reduce(action);
            else if (action.equals(ParserTables.SHIFT))
                token = shift(token);

            action = recover(action);    // recover if error
        }

        return detectError(token, top(), action);
    }


    protected Integer recover(Integer action) {
        return action;
    }

    protected boolean detectError(Token token, Integer state, Integer action) {
        boolean ret = true;

        if (token.symbol == null || action.equals(ParserTables.ERROR)) {
            if (token.symbol == null)
                ensureOut().println("ERROR: Unknown symbol: >" + token.text + "<, state " + state);
            else
                ensureOut().println("ERROR: Wrong symbol: " + (Token.isEpsilon(token) ? "EOF" : token.symbol + ", text: >" + token.text + "<") + ", state " + state);

            lexer.dump(out);

            Map h = getParserTables().getExpected(state);
            if (h != null) {
                ensureOut().print("Expected was (one of): ");

                for (Iterator it = h.keySet().iterator(); it.hasNext(); ) {
                    String s = (String) it.next();
                    ensureOut().print((Token.isEpsilon(s) ? "EOF" : s) + (it.hasNext() ? ", " : ""));
                }
                ensureOut().println();
            }

            ret = false;
        } else if (state.equals(ParserTables.ERROR)) {    // ERROR lies on stack, from SHIFT
            pop(1);
            ensureOut().println("ERROR: found no possible follow state for " + top() + ", text >" + token.text + "<");
            lexer.dump(out);
            ret = false;
        } else if (!Token.isEpsilon(token)) {
            ensureOut().println("ERROR: Input is not finished.");
            lexer.dump(out);
            ret = false;
        } else if (!action.equals(ParserTables.ACCEPT)) {
            ensureOut().println("ERROR: Could not achieve ACCEPT. Symbol: " + token.symbol);
            lexer.dump(out);
            ret = false;
        }

        if (!ret)
            result = null;

        return ret;
    }


    private void clear() {
        stateStack.removeAllElements();
        valueStack.removeAllElements();
        rangeStack.removeAllElements();
        range = new Token.Range(null, null);
        inputTokens = null;
        result = null;
        if (lexer != null)
            lexer.clear();
    }


    private void dumpStack() {
        if (DEBUG) {
            ensureOut().print("stack: ");
            for (int i = 0; i < stateStack.size(); i++)
                ensureOut().print(stateStack.elementAt(i) + " ");
            ensureOut().println();
        }
    }

    private void dump(String s) {
        ensureOut().println(s);
    }

    private PrintStream ensureOut() {
        if (out == null)
            out = System.err;
        return out;
    }


    public void setPrintStream(PrintStream out) {
        this.out = (out != null) ? out : System.err;
    }


    public void setDebug(boolean debug) {
        DEBUG = debug;
    }

}
