package parser.examples;

import parser.Lexer;
import parser.Parser;
import parser.ParserTables;
import parser.Token;
import parser.parsertables.FileReader.dummyFileReader;
import parser.parsertables.LRParserTables;
import parser.syntax.builder.SyntaxBuilder;
import semantic.TreeBuilderSemantic;

import java.io.*;
import java.net.URL;
import java.util.Date;


public class JavaParser {

    public static String absPath = "/home/tomas/semAnalyzer/semanalyzator/src/main/";
    public static void main(String[] args) throws Exception {


        InputStream inputStream = new FileInputStream(absPath+"resources/pirmaTM.syntax");
        Reader syntaxInput = new InputStreamReader(inputStream);
        File lastModifiedResourceUrl = new File(absPath+"resources/pirmaTM.syntax");
        Date lastModifiedSyntax = new Date(lastModifiedResourceUrl.lastModified());

        SyntaxBuilder builder = new SyntaxBuilder(syntaxInput);
        Lexer lexer = builder.getLexer();
        lexer.setDebug(true);

        dummyFileReader dummy = new dummyFileReader(absPath+"resources/DumpedParse.txt"
                , absPath+"resources/DumpedGoto.txt", lastModifiedSyntax);
        dummy.setLastModified(lastModifiedSyntax);

        ParserTables tables = new LRParserTables(dummy.getReadedParseTables(),
                dummy.getReadedGotoTables(),builder.getParserSyntax());


        Parser parser = new Parser(tables);


        parser.setLexer(lexer);

        parser.getLexer().addTokenListener((token, ignored) -> {
            if (ignored) {
                System.err.println("------------- Ignored Token Received -------------------");
                System.err.println(token.text);
                System.err.println("--------------------------------------------------------");

            }
        });

        PrintStream fileOutput = new PrintStream(
                new FileOutputStream(absPath+"resources/DumpedParse.txt", true));
        PrintStream fileOutput1 = new PrintStream(
                new FileOutputStream(absPath+"resources/DumpedGoto.txt", true));

        parser.getParserTables().toFile(fileOutput,fileOutput1,lastModifiedSyntax);

        String fileToParse = absPath+"java/parser/examples/exampleProgram";
        FileReader parseInput = new FileReader(fileToParse);
        System.err.println("========================================================");
        System.err.println("Parsing: " + fileToParse);

        parser.setInput(parseInput);
        boolean ok = false;
        if (ok = parser.parse(lexer, new TreeBuilderSemantic())) {
            TreeBuilderSemantic.Node n = (TreeBuilderSemantic.Node) parser.getResult();
            System.err.println("got result: " + n);
            System.out.println(n.toString(0));
        }

    }


}
