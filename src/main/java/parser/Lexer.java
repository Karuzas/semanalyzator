package parser;

import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.Map;


public interface Lexer {

    void setInput(Object text) throws IOException;


    void setTerminals(List terminals);


    Token getNextToken(Map tokenSymbols) throws IOException;


    void clear();


    interface TokenListener {


        void tokenReceived(Token token, boolean ignored);
    }


    void addTokenListener(Lexer.TokenListener listener);


    void removeTokenListener(Lexer.TokenListener listener);


    // debug methods


    void dump(PrintStream out);


    void setDebug(boolean debug);

}
