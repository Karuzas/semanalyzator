package parser.parsertables;

import parser.Token;
import parser.syntax.Rule;
import parser.syntax.Syntax;
import util.UniqueAggregatingHashtable;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;


class FirstSets extends UniqueAggregatingHashtable {

    public FirstSets(Syntax syntax, Nullable nullAble, List nonterminals)
            throws ParserBuildException {
        Map done = new Hashtable(nonterminals.size());    // avoid recursion
        for (int i = 0; i < nonterminals.size(); i++) {
            String nonterm = (String) nonterminals.get(i);
            generateFirstSet(syntax, nullAble, nonterm, done);
        }
    }

    private void generateFirstSet(Syntax syntax, Nullable nullAble, String nonterminal, Map done)
            throws ParserBuildException {
        if (get(nonterminal) != null)
            return;    // alreay done

        if (done.get(nonterminal) != null)
            return;
        done.put(nonterminal, nonterminal);    // avoid endless loops
        //System.err.println("generateFirstSet("+nonterminal+")");

        for (int k = 0; k < syntax.size(); k++) {
            Rule rule = syntax.getRule(k);
            String nonterm = rule.getNonterminal();    // left side of derivation

            if (nonterminal.equals(nonterm)) {    // this rule derives the nonterminal
                // if left side is empty, add NULL to FIRST of nonterminal
                if (rule.rightSize() <= 0) {
                    put(nonterm, Nullable.NULL);
                } else {    // there are symbols on left side
                    boolean nullable = true;    // enable loop until nullable

                    // While nullable, add the symbol, shift to the next and
                    // check if it is nullable again.
                    for (int i = 0; nullable && i < rule.rightSize(); i++) {
                        String symbol = rule.getRightSymbol(i);

                        nullable = false;    // assume it is a terminal

                        if (Token.isTerminal(symbol)) {
                            put(nonterm, symbol);
                        } else {
                            // If there is a nonterminal on first position, add its FIRST set
                            // FIRST set of this nonterminal, but without null-word.
                            try {
                                generateFirstSet(syntax, nullAble, symbol, done);    // enter recursion

                                List list = (List) get(symbol);    // get the results
                                for (int j = 0; list != null && j < list.size(); j++) {
                                    String s = (String) list.get(j);
                                    put(nonterm, s);
                                }

                                nullable = nullAble.isNullable(symbol);
                            } catch (Exception ex) {
                                throw new ParserBuildException(ex.getMessage() + " <- " + nonterm);
                            }
                        }    // end if terminal
                    }    // end for all symbols of rule
                }    // end if rule size > 1
            }    // end for al rules of syntax
        }
    }


    public Object put(Object key, Object value) {
        if (key.equals(value))
            throw new IllegalArgumentException("Can not be FIRST of its own: key=" + key + ", value=" + value);

        return super.put(key, value);
    }


    public static void main(String[] args) {
        // nonterminals
        List nonterm = new ArrayList();
        nonterm.add("S");
        nonterm.add("T");
        nonterm.add("F");
        nonterm.add("L");

        // rules
        List sx = new ArrayList();

        List r = new ArrayList();
        r.add("S");
        r.add("T");
        r.add("'*'");
        r.add("F");
        sx.add(r);

        r = new ArrayList();
        r.add("S");
        r.add("T");
        sx.add(r);

        r = new ArrayList();
        r.add("T");
        r.add("F");
        sx.add(r);


        r = new ArrayList();
        r.add("F");
        sx.add(r);

        r = new ArrayList();
        r.add("F");
        r.add("'1'");
        sx.add(r);

        Syntax syntax = new Syntax(sx);
        try {
            FirstSets f = new FirstSets(syntax, new Nullable(syntax, nonterm), nonterm);
            String s = "S";
            System.err.println("FIRST(" + s + ") = " + f.get(s));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}