package parser.parsertables;

import parser.Token;
import parser.examples.JavaParser;
import parser.syntax.Rule;
import parser.syntax.Syntax;

import java.io.*;
import java.net.URL;
import java.util.*;


public class LRPrimitiveTables extends AbstractParserTables {
    protected transient List syntaxNodes = new ArrayList();
    protected transient FirstSets firstSets;
    protected transient FollowSets followSets;


    public LRPrimitiveTables(Syntax syntax)
            throws ParserBuildException {
        this.syntax = addStartSymbol(syntax);    // add START symbol to begin
        getAllSymbols();
        init();
    }

    public LRPrimitiveTables(ArrayList<Hashtable<String, Integer>> parseTables,
                             ArrayList<Hashtable<String, Integer>> gotoTables,Syntax syntax)
            throws ParserBuildException {
        this.syntax = addStartSymbol(syntax);    // add START symbol to begin
        this.parseTable = parseTables;
        this.gotoTable = gotoTables;
        getAllSymbols();
        init();
    }


    protected void init()
            throws ParserBuildException {
        syntaxNodes = new LRPrimitive().build(syntax, syntaxNodes, new Hashtable());
        gotoTable = generateGoto(syntaxNodes);

        Nullable nullAble = new Nullable(syntax, nonterminals);
        firstSets = new FirstSets(syntax, nullAble, nonterminals);
        followSets = new FollowSets(syntax, nullAble, firstSets);

    }



    // Looks for top level rules and inserts a START rule pointing to it.
    // Inserts a default rule pointing to the first rule when none found.
    private Syntax addStartSymbol(Syntax syntax)
            throws ParserBuildException {
        String startSym = null;
        List startRules = syntax.findStartRules();

        if (startRules.size() <= 0) {    // no toplevel rule
            Rule rule = syntax.getRule(0);
            System.err.println("WARNING: Grammar has no top level rule, taking first rule >" + rule + "<");
            startSym = rule.getNonterminal();
        } else if (startRules.size() > 1) {    // more than one toplevel rules
            for (int i = 0; i < startRules.size(); i++) {    // check if all start rules have the same nonterminal on left side
                Rule r = (Rule) startRules.get(i);
                String nt = r.getNonterminal();

                if (startSym == null)
                    startSym = nt;
                else if (startSym.equals(nt) == false)
                    throw new ParserBuildException("Grammar has more than one toplevel rules: " + startRules);
            }
        } else {    // exactly one toplevel rule found
            startSym = ((Rule) startRules.get(0)).getNonterminal();
        }

        Rule start = new Rule("<START>", 1);
        start.addRightSymbol(startSym);
        syntax.insertRule(0, start);

        return syntax;
    }


    protected List getAllSymbols()
            throws ParserBuildException {
        // collect nonterminals unique
        for (int i = 0; i < syntax.size(); i++) {
            Rule rule = syntax.getRule(i);
            String nonterm = rule.getNonterminal();    // left side of derivation

            if (Nullable.isNull(nonterm))
                throw new ParserBuildException("ERROR: Empty nonterminal: >" + nonterm + "<");

            if (nonterminals.indexOf(nonterm) < 0)    // insert unique
                nonterminals.add(nonterm);
        }

        // collect terminals unique
        for (int j = 0; j < syntax.size(); j++) {
            Rule rule = syntax.getRule(j);

            for (int i = 0; i < rule.rightSize(); i++) {
                String symbol = rule.getRightSymbol(i);

                if (Nullable.isNull(symbol))
                    throw new ParserBuildException("ERROR: Empty terminal: >" + symbol + "<");

                if (Token.isTerminal(symbol)) {
                    if (terminals.indexOf(symbol) < 0) {
                        terminals.add(symbol);
                        terminalsWithoutEpsilon.add(symbol);
                    }
                } else    // throw error if nonterminal is not present
                    if (nonterminals.indexOf(symbol) < 0)
                        throw new ParserBuildException("ERROR: Every nonterminal must have a rule. symbol: >" + symbol + "<, rule: " + rule);
            }
        }

        // check for sanity
        if (terminals.size() <= 0)
            throw new ParserBuildException("ERROR: No terminal found: " + syntax);

        if (nonterminals.size() <= 0)
            throw new ParserBuildException("ERROR: No nonterminal found: " + syntax);

        // add nonterminals to symbols
        for (int i = 0; i < nonterminals.size(); i++)
            symbols.add(nonterminals.get(i));

        // add terminals without EpSiLoN to symbols
        for (int i = 0; i < terminals.size(); i++)
            symbols.add(terminals.get(i));

        // add "Epsilon" Symbol to Terminals
        terminals.add(Token.EPSILON);

        return symbols;
    }


    protected List generateGoto(List syntaxNodes) {

        System.err.println("got "+syntaxNodes.size()+" states");
        this.gotoTable = new ArrayList(syntaxNodes.size());

        Map<Map, Integer> hash = new Hashtable<Map, Integer>(syntaxNodes.size());

        for (int i = 0; i < syntaxNodes.size(); i++) {
            LRPrimitive node = (LRPrimitive) syntaxNodes.get(i);

            Map h = node.fillGotoLine(i);

            if (h.size() <= 0)
                gotoTable.add(null);
            else
                insertTableLine(i, h, gotoTable, hash);
        }

        return gotoTable;
    }


    protected List generateParseAction(List syntaxNodes) {

        this.parseTable = new ArrayList(syntaxNodes.size());

        Map<Map, Integer> hash = new Hashtable<Map, Integer>(syntaxNodes.size());


        for (int i = 0; i < syntaxNodes.size(); i++) {

            LRPrimitive node = (LRPrimitive) syntaxNodes.get(i);


            Map h = node.fillParseActionLine(i, firstSets, followSets);

            if (h.size() <= 0)
                parseTable.add(null);
            else
                insertTableLine(i, h, parseTable, hash);
        }

        return parseTable;
    }

    protected void insertTableLine(int i, Map line, List table, Map<Map, Integer> hash) {
        Integer itg = hash.get(line);
        if (itg == null) {
            table.add(line);
            hash.put(line, i);
        } else {
            table.add(table.get(itg));
        }
    }


    public void freeSyntaxNodes() {
        syntaxNodes = null;
        symbols = null;
        terminals = null;
    }


    // dump methods to print parser information


    public void report(PrintStream out) {
        System.err.println("Parser Generator is " + getClass());
        super.report(out);
        out.println("states: " + (syntaxNodes != null ? syntaxNodes.size() : -1));
    }

    /*
        Created methods to dump table in some kind of stucture ()
        Added by Tomas.
     */
    public void toFile(PrintStream output,PrintStream output1, Date lastModified){
        FileWriter configFile;

        if(!this.readedFromFile[0]) {
            for (int i = 0; i < parseTable.size(); i++) {
                String entry = parseTable.get(i).toString();
                output.println(entry.substring(1, entry.length() - 1));
            }
            try {
                configFile = new FileWriter(new File(JavaParser.absPath+"resources/Config.dat"),false);
                configFile.write(lastModified+"");
                configFile.close();
            }catch (IOException e){
                System.err.println(e);
            }
        }else{
            System.out.println("INFO: Not exporting to file PARSE-ACTION tables.");
        }
        if (!this.readedFromFile[1]) {
            for (int z = 0; z < gotoTable.size(); z++) {
                output1.println((gotoTable.get(z) != null) ?
                        gotoTable.get(z).toString().substring(1, gotoTable.get(z).toString().length() - 1) : "null");
            }
            try {
                new FileWriter(new File(JavaParser.absPath+"resources/Config.dat"),true).write(lastModified+"");
            }catch (IOException e){
                System.err.println(e);
            }
        }else{
            System.out.println("INFO: Not exporting to file GO-TO tables.");
        }
        if (new File(JavaParser.absPath+"resources/Config.dat") == null){
            try {
                configFile = new FileWriter(new File("Config.dat"),false);
                configFile.write(lastModified+"");
                configFile.close();
            }catch (IOException e){
                System.err.println(e);
            }
        }
    }


    public void setParserTables(ArrayList<HashMap<String, Integer>> x) {
        this.parseTable = x;
    }

    public void dump(PrintStream out) {
        dumpFollowSet(out);
        dumpSyntaxNodes(out);
        dumpFirstSet(out);
        dumpGoto(out);
        dumpParseAction(out);
    }

    public void dumpSyntaxNodes(PrintStream out) {
        if (syntaxNodes != null) {
            for (int i = 0; i < syntaxNodes.size(); i++)
                dumpSyntaxNode(i, (LRPrimitive) syntaxNodes.get(i), out);
            out.println();
        }
    }

    public void dumpSyntaxNode(int i, LRPrimitive node, PrintStream out) {
        out.println("State " + i);
        out.println(node.toString());
    }

    public void dumpFirstSet(PrintStream out) {
        if (firstSets != null)
            dumpSet("FIRST", firstSets, out);
    }

    public void dumpFollowSet(PrintStream out) {
        if (followSets != null)
            dumpSet("FOLLOW", followSets, out);
    }

    public void dumpSet(String header, Map set, PrintStream out) {
        for (Iterator it = set.keySet().iterator(); it.hasNext(); ) {
            String nonterm = (String) it.next();
            out.println(header + "(" + nonterm + ") = " + set.get(nonterm));
        }
        out.println();
    }


}
