package parser.parsertables.FileReader;

import parser.examples.JavaParser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;


/**
 * Created by Tomas on 11/23/15.
 */
public class dummyFileReader {

    private final int typeParseTable = 0;
    //private final int typeGotoTable = 1; Unused. Would be used if there would e other tables exportation
    private ArrayList<Hashtable<String, Integer>> readedParseTables = null;
    private ArrayList<Hashtable<String,Integer>>  readedGotoTables = null;
    private Date lastModifiedSyntax;

    public dummyFileReader(String parserTables, String gotoTables, Date lastModifiedSyntaxIn){
        FileReader parserTablesFile = openFile(parserTables);
        FileReader gotoTablesFile = openFile(gotoTables);
        FileReader config = openFile(JavaParser.absPath+"resources/Config.dat");
        lastModifiedSyntax = lastModifiedSyntaxIn;

        if (config != null){
                BufferedReader buffReader = new BufferedReader(config);
                String currLine;
            try {
                if ((currLine = buffReader.readLine()) != null) {
                    DateFormat format = new SimpleDateFormat("EEE MMM d kk:mm:ss zzz yyyy", Locale.ENGLISH);
                    Date tempDate = null;
                    try{
                        tempDate = format.parse(currLine);
                    }catch (ParseException e){
                        System.err.println(e);
                    }
                    if((tempDate != null) && tempDate.toString().equals(lastModifiedSyntax.toString())) {
                        if (gotoTablesFile != null) {
                            readedGotoTables = readTable(gotoTablesFile, 1);
                            System.out.println("Readed from GO-TO");
                        }
                        if (parserTablesFile != null) {
                            readedParseTables = readTable(parserTablesFile, 0);
                            System.out.println("Readed from PARSE-ACTION");
                        }
                    }

                }
            }catch (IOException e){
                System.err.println("Error." + e);
            }
        }



    }

    public void setLastModified(Date lastModifiedIn){
        lastModifiedSyntax = lastModifiedIn;
    }

    private FileReader openFile(String inputFile){
       FileReader retVal = null;
        try{
            retVal = new FileReader(inputFile);
        } catch(IOException e){
            System.err.println("File not found. "+ inputFile +  " Exception on: " +
                    this.getClass().getSimpleName());
        }
        return retVal;
    }

    private ArrayList<Hashtable<String, Integer>> readTable(FileReader input, int type){
        ArrayList<Hashtable<String, Integer>> retVal = new ArrayList<Hashtable<String, Integer>>();
        try {
            BufferedReader buffReader = new BufferedReader(input);
            if (type == typeParseTable) {
                String thisLine;
                int tableLine = 0;
                while ((thisLine = buffReader.readLine()) != null) {
                    tableLine++;
                    String[] splittedComma = thisLine.split(", ");
                    Hashtable<String, Integer> tableEntry = new Hashtable<String,Integer>(splittedComma.length);
                    for (int i = 0; i < splittedComma.length; i++) {
                        String[] splittedEquals = splittedComma[i].split("=(?=[\\d{1,} | -])");
                        try {
                            tableEntry.put(splittedEquals[0], Integer.parseInt(splittedEquals[1]));
                        }catch(NumberFormatException e){
                            System.err.println("Got exception while reading file from system on line: " + tableLine +
                                    " \n" + e);
                        }
                    }
                    retVal.add(tableEntry);
                }
            }else{
                String thisLine;
                int tableLine = 0;
                while ((thisLine = buffReader.readLine()) != null) {
                    tableLine++;
                    if (!thisLine.equals("null")) {
                        String[] splittedComma = thisLine.split(", ");
                        Hashtable<String, Integer> tableEntry = new Hashtable<String, Integer> (splittedComma.length);
                        for (int i = 0; i < splittedComma.length; i++) {
                            String[] splittedEquals = splittedComma[i].split("=(?=[\\d{1,} | -])");
                            try {
                                if (splittedEquals.length > 0) {
                                    tableEntry.put(splittedEquals[0], Integer.parseInt(splittedEquals[1]));
                                } else {
                                    //tableEntry.put(null);
                                }
                            } catch (NumberFormatException e) {
                                System.err.println("Got exception while reading file from system on line: " + tableLine +
                                        " \n" + e);
                            }
                        }
                        retVal.add(tableEntry);
                    }else{
                        retVal.add(null);
                    }
                }

            }
            }catch(IOException e){
                System.err.println("File corrupted. ");
            }

        return retVal;
    }

    public ArrayList<Hashtable<String, Integer>> getReadedParseTables(){
        return readedParseTables;
    }

    public ArrayList<Hashtable<String, Integer>> getReadedGotoTables(){
        return readedGotoTables;
    }


    public static void main(String[] args) {
        dummyFileReader z = new dummyFileReader("DumpedParse.txt", "DumpedGoto.txt", null);
        System.out.println(z.getReadedGotoTables().get(5).toString());
    }
}
