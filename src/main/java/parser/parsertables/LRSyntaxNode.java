package parser.parsertables;

import parser.Token;
import parser.syntax.Rule;
import parser.syntax.Syntax;

import java.util.*;


class LRSyntaxNode extends LRPrimitive {

    protected Nullable nullable;


    protected FirstSets firstSets;


    public LRSyntaxNode(Nullable nullable, FirstSets firstSets) {
        this.nullable = nullable;
        this.firstSets = firstSets;
    }


    protected LRPrimitive createSyntaxNode() {
        return new LRSyntaxNode(nullable, firstSets);
    }


    protected RuleStateItem createRuleStateItem(int ruleIndex, Rule rule) {
        LRRuleStateItem item = new LRRuleStateItem(ruleIndex, rule);
        addStartLookahead(item, ruleIndex);
        return item;
    }


    protected void addStartLookahead(LRRuleStateItem item, int ruleIndex) {
        if (ruleIndex == 0) {
            List list = new ArrayList();
            list.add(Token.EPSILON);
            item.addLookahead(list.iterator());
        }
    }


    protected void addRulesDerivingPendingNonTerminal(RuleStateItem itm, String nonterm, Syntax syntax, List newItems) {
        // make the closure for one item:
        // if pointer before a nonterminal, add all rules that derive it

        LRRuleStateItem item = (LRRuleStateItem) itm;
        List lookahead = null;

        for (int i = 0; i < syntax.size(); i++) {
            Rule rule = syntax.getRule(i);

            if (rule.getNonterminal().equals(nonterm)) {
                LRRuleStateItem rsi = (LRRuleStateItem) createRuleStateItem(i, rule);

                if (lookahead == null)    // calculate lookahead, all new items get the same lookahead
                    item.calculateLookahead(
                            lookahead = new ArrayList(),
                            nullable,
                            firstSets);

                rsi.addLookahead(lookahead.iterator());    // merge lookaheads

                // look if new item is already contained, add when not
                if (entries.containsKey(rsi) == false) {
                    entries.put(rsi, rsi);
                    newItems.add(rsi);
                }
            }
        }
    }


    protected List getNontermShiftSymbols(FirstSets firstSets, String nonterm) {
        return null;
    }


    protected Iterator getReduceSymbols(FollowSets followSets, RuleStateItem item) {
        return ((LRRuleStateItem) item).lookahead.keySet().iterator();
    }


    protected class LRRuleStateItem extends RuleStateItem {
        Hashtable lookahead = new Hashtable();

        public LRRuleStateItem(int ruleIndex, Rule rule) {
            super(ruleIndex, rule);
        }


        protected LRRuleStateItem(RuleStateItem orig) {
            super(orig);
            lookahead = (Hashtable) ((LRRuleStateItem) orig).lookahead.clone();
        }


        protected RuleStateItem createRuleStateItem(RuleStateItem orig) {
            return new LRRuleStateItem(orig);
        }


        boolean addLookahead(Iterator propagation) {
            // merge lookaheads
            boolean ret = false;    // assume no changes

            while (propagation.hasNext()) {
                Object la = propagation.next();

                if (lookahead.get(la) == null) {
                    lookahead.put(la, la);
                    ret = true;    // there were changes
                }
            }
            return ret;
        }


        boolean calculateLookahead(List newLookahead, Nullable nullable, FirstSets firstSets) {
            // consider all nullable symbols after the one past the dot
            // and add their first symbols to lookahead set.

            for (int i = pointerPosition; i < rule.rightSize(); i++) {    // when pointer at start, it has value 1
                String symbol = rule.getRightSymbol(i);

                if (Token.isTerminal(symbol)) {
                    newLookahead.add(symbol);
                    return false;    // originator lookahead not visible
                } else {
                    List firstSet = (List) firstSets.get(symbol);

                    for (int j = 0; j < firstSet.size(); j++) {
                        String la = (String) firstSet.get(j);
                        newLookahead.add(la);
                    }

                    if (nullable.isNullable(symbol) == false)
                        return false;
                }
            }

            // if we get here everything was nullable, add all lookaheads of this item
            for (Enumeration e = lookahead.keys(); e.hasMoreElements(); )
                newLookahead.add(e.nextElement());

            return true;    // originator lookahead is visible
        }


        public boolean equals(Object o) {
            if (super.equals(o) == false)
                return false;

            LRRuleStateItem item = (LRRuleStateItem) o;
            return item.lookahead.equals(lookahead) != false;

        }


        public int hashCode() {
            if (hashCache == null) {
                int result = 0;
                for (Enumeration e = lookahead.keys(); e.hasMoreElements(); )
                    result ^= e.nextElement().hashCode();
                hashCache = new Integer(ruleIndex * 13 + pointerPosition + result);
            }
            return hashCache.intValue();
        }


        public String toString() {
            String s = super.toString();
            int i = s.lastIndexOf("->");
            if (i > 0)
                s = s.substring(0, i) + "LOOKAHEAD" + hashToStr(lookahead) + "	" + s.substring(i);
            else
                s = s + "	" + hashToStr(lookahead);
            return s;
        }

        // output of hashtable keys, separated by ", ".
        private String hashToStr(Hashtable l) {
            StringBuffer sb = new StringBuffer("[");
            for (Enumeration e = l.keys(); e.hasMoreElements(); ) {
                String s = (String) e.nextElement();
                sb.append(s);
                if (e.hasMoreElements())
                    sb.append(", ");
                else if (l.size() == 1 && s.length() <= 0)    // only Epsilon
                    sb.append(" ");
            }
            sb.append("]");
            return sb.toString();
        }

    }    // end class RuleStateItem

}