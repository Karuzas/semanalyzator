package parser.parsertables;

public class ParserBuildException extends Exception {
    public ParserBuildException(String msg) {
        super(msg);
    }
}

	