package parser.parsertables;

import parser.Token;
import parser.syntax.Rule;
import parser.syntax.Syntax;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;


class Nullable extends Hashtable {

    public static final String NULL = "";


    public Nullable(Syntax syntax, List nonterminals)
            throws ParserBuildException {
        // loop nonterminals
        Map done = new Hashtable(nonterminals.size());    // avoid endless loops
        for (int i = 0; i < nonterminals.size(); i++) {
            String nt = (String) nonterminals.get(i);
            checkNullability(syntax, nt, done);
        }
    }


    public boolean isNullable(String nonterminal) {
        Boolean nullable = (Boolean) get(nonterminal);
        return nullable.booleanValue();
    }


    private boolean checkNullability(Syntax syntax, String nonterm, Map done)
            throws ParserBuildException {
        Boolean n = (Boolean) get(nonterm);
        if (n != null)
            return n.booleanValue();

        if (done.get(nonterm) != null)
            return false;    // endless recursion

        done.put(nonterm, nonterm);    // avoid endless loops

        // loop rules for an empty rule deriving this nonterminal
        for (int j = 0; j < syntax.size(); j++) {
            Rule rule = syntax.getRule(j);
            String nt = rule.getNonterminal();    // left side of derivation

            if (nt.equals(nonterm) && rule.rightSize() <= 0)    // this rule derives the nonterminal and is empty
                return putSymbol(nonterm, true);
        }

        // loop rules for nullable nonterminal sequences
        for (int j = 0; j < syntax.size(); j++) {
            Rule rule = syntax.getRule(j);
            String nt = rule.getNonterminal();    // left side of derivation

            if (nt.equals(nonterm)) {    // this rule derives the nonterminal
                boolean nullable = true;    // assume it is nullable

                // then all symbols on right side must be nullable
                for (int i = 0; nullable && i < rule.rightSize(); i++) {
                    String symbol = rule.getRightSymbol(i);

                    if (Token.isTerminal(symbol)) {    // a terminal ends symbol-loop
                        nullable = false;
                    } else if (symbol.equals(nonterm) == false) {    // do not search self
                        try {
                            nullable = checkNullability(syntax, symbol, done);    // is nullable when this symbol is nullable
                        } catch (Exception ex) {
                            throw new ParserBuildException("Nullable ERROR: " + ex.getMessage() + " <- " + nonterm);
                        }
                    }
                }

                if (nullable)    // one nullable rule is enough for nonterminal to be nullable
                    return putSymbol(nonterm, true);
            }
        }
        return putSymbol(nonterm, false);
    }

    private boolean putSymbol(String symbol, boolean value) {
        put(symbol, new Boolean(value));
        return value;
    }


    public static boolean isNull(String symbol) {
        return symbol.length() <= 0 ||
                symbol.equals("" + Token.STRING_QUOTE + Token.STRING_QUOTE) ||
                symbol.equals("" + Token.COMMAND_QUOTE + Token.COMMAND_QUOTE) ||
                symbol.equals("" + Token.CHAR_QUOTE + Token.CHAR_QUOTE);
    }


}