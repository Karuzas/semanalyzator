package parser.parsertables;

import parser.ParserTables;
import parser.Token;
import parser.syntax.Rule;
import parser.syntax.Syntax;

import java.util.*;


class LRPrimitive {

    protected Hashtable entries = new Hashtable();

    private int kernels = 0;
    private Integer hashCache = null;


    public LRPrimitive() {
    }


    protected LRPrimitive createSyntaxNode() {
        return new LRPrimitive();
    }


    protected RuleStateItem createRuleStateItem(int ruleIndex, Rule rule) {
        return new RuleStateItem(ruleIndex, rule);
    }


    public List build(Syntax syntax, List syntaxNodes, Hashtable kernels) {
        // insert first rule as state item
        RuleStateItem item = createRuleStateItem(0, syntax.getRule(0));
        entries.put(item, item);
        closure(syntax);    // calculate followers

        syntaxNodes.add(this);    // now add startnode to list of syntax nodes

        generateSyntaxNodes(syntaxNodes, syntax, kernels);    // generate all other nodes

        //System.err.println("Built "+syntaxNodes.size()+" states.");
        return syntaxNodes;
    }


    protected void generateSyntaxNodes(List syntaxNodes, Syntax syntax, Hashtable kernels) {
        // newly appended nodes will be found and processed
        for (int i = 0; i < syntaxNodes.size(); i++) {
            LRPrimitive node = (LRPrimitive) syntaxNodes.get(i);
            node.generateSyntaxNodesFromItems(syntaxNodes, syntax, kernels);
        }
    }


    protected void generateSyntaxNodesFromItems(List syntaxNodes, Syntax syntax, Hashtable kernels) {
        for (Enumeration e = entries.elements(); e.hasMoreElements(); ) {
            RuleStateItem item = (RuleStateItem) e.nextElement();
            String pending = item.getPendingSymbol();

            if (item.followNodeIndex < 0 && pending != null) {    // further states are possible
                // create a kernel item
                LRPrimitive node = createSyntaxNode();
                List tmp = node.addShiftedItems(pending, entries);    // get entries that have been taken

                // look if it is already in list
                Integer kernelIndex = (Integer) kernels.get(node);
                int index = kernelIndex != null ? kernelIndex.intValue() : -1;

                // if not in list, add it, compute closure
                if (index < 0) {
                    index = syntaxNodes.size();
                    kernels.put(node, new Integer(index));
                    syntaxNodes.add(node);
                    node.closure(syntax);
                }

                // link originator entries to new or found node
                for (int i = 0; i < tmp.size(); i++) {
                    Tuple t = (Tuple) tmp.get(i);
                    linkParentItemToChild(t.o1, index);
                }
            }
        }
    }


    protected List addShiftedItems(String symbol, Hashtable originatorEntries) {
        List list = new ArrayList();
        for (Enumeration e = originatorEntries.elements(); e.hasMoreElements(); ) {
            RuleStateItem item = (RuleStateItem) e.nextElement();
            String pending = item.getPendingSymbol();

            if (pending != null && symbol.equals(pending)) {
                RuleStateItem newitem = item.shift();
                this.entries.put(newitem, newitem);
                list.add(new Tuple(item, newitem));    // return all derived originator items
            }
        }

        kernels = list.size();    // remember count of kernel items

        return list;    // return list of entries that were shifted
    }


    protected void linkParentItemToChild(RuleStateItem parent, int newIndex) {
        parent.followNodeIndex = newIndex;
    }


    protected void closure(Syntax syntax) {
        // put Hashtable to List for sequential work
        List todo = new ArrayList(entries.size() * 2);
        for (Enumeration e = entries.elements(); e.hasMoreElements(); )
            todo.add(e.nextElement());

        // loop todo list and find every added new item
        for (int i = 0; i < todo.size(); i++) {
            RuleStateItem rsi = (RuleStateItem) todo.get(i);
            String nonterm = rsi.getPendingNonTerminal();
            if (nonterm != null)
                addRulesDerivingPendingNonTerminal(rsi, nonterm, syntax, todo);
        }
    }


    protected void addRulesDerivingPendingNonTerminal(RuleStateItem item, String nonterm, Syntax syntax, List todo) {
        // make the closure for one item:
        // if pointer before a nonterminal, add all rules that derive it
        for (int i = 0; i < syntax.size(); i++) {
            Rule rule = syntax.getRule(i);

            if (rule.getNonterminal().equals(nonterm)) {
                RuleStateItem rsi = createRuleStateItem(i, rule);

                if (entries.containsKey(rsi) == false) {
                    entries.put(rsi, rsi);    // real entry list
                    todo.add(rsi);    // work list
                }
            }
        }
    }


    public Hashtable fillGotoLine(int state) {
        Hashtable h = new Hashtable(entries.size() * 3 / 2);    // load factor 0.75

        // fill one row of GOTO-table
        for (Enumeration e = entries.elements(); e.hasMoreElements(); ) {
            // store temporary
            RuleStateItem item = (RuleStateItem) e.nextElement();
            String symbol = item.getPendingSymbol();

            if (symbol != null) {    // if pointer not at end of rule
                //System.err.println("This time:	"+item);
                setTableLine("GOTO", state, h, item, new Integer(item.followNodeIndex), symbol);
            }
        }
        return h;
    }


    public Hashtable fillParseActionLine(int state, FirstSets firstSets, FollowSets followSets) {
        // fill one row of PARSE-ACTION-table
        Hashtable h = new Hashtable(entries.size() * 10);

        for (Enumeration e = entries.elements(); e.hasMoreElements(); ) {
            // store temporary
            RuleStateItem item = (RuleStateItem) e.nextElement();
            String symbol = item.getPendingSymbol();

            if (symbol != null) {    // pointer not at end of rule, SHIFT
                if (Token.isTerminal(symbol)) {    // enter SHIFT at terminal symbol
                    // first-set of terminal is terminal
                    setParseTableLine(state, h, item, ParserTables.SHIFT, symbol);
                } else {    // put SHIFT at all terminals of FIRST-set
                    List firstSet = getNontermShiftSymbols(firstSets, item.getNonterminal());

                    if (firstSet != null) {    // LALR will return null, SLR not null
                        for (int i = 0; i < firstSet.size(); i++) {
                            String terminal = (String) firstSet.get(i);
                            setParseTableLine(state, h, item, ParserTables.SHIFT, terminal);
                        }
                    }
                }
            } else {    // pointer at end, REDUCE to rule number
                for (Iterator reduceSymbols = getReduceSymbols(followSets, item); reduceSymbols.hasNext(); ) {
                    String terminal = (String) reduceSymbols.next();

                    if (item.ruleIndex == 0)    // is startnode
                        setParseTableLine(state, h, item, ParserTables.ACCEPT, terminal);
                    else    // ruleIndex > 0 means REDUCE
                        setParseTableLine(state, h, item, new Integer(item.ruleIndex), terminal);
                }
            }
        }
        return h;
    }


    protected List getNontermShiftSymbols(FirstSets firstSets, String nonterm) {
        return (List) firstSets.get(nonterm);
    }


    protected Iterator getReduceSymbols(FollowSets followSets, RuleStateItem item) {
        return ((List) followSets.get(item.getNonterminal())).iterator();
    }


    protected void setParseTableLine(int state, Hashtable line, RuleStateItem item, Integer action, String terminal) {
        // set one action into a parse-table row and resolve conflicts

        if (setTableLine("PARSE-ACTION", state, line, item, action, terminal) == false) {
            // shift/reduce or reduce/reduce conflict
            Object o = line.get(terminal);

            if (action.equals(ParserTables.SHIFT) || o.equals(ParserTables.SHIFT)) {
                // prefer SHIFT operation
                line.put(terminal, ParserTables.SHIFT);
                System.err.println("WARNING: shift/reduce conflict, SHIFT is preferred.");
            } else {
                System.err.println("WARNING: reduce/reduce conflict, rule with smaller index is preferred.");
                // prefer rule with smaller index
                if (((Integer) o).intValue() > action.intValue())
                    line.put(terminal, action);
            }
        }
    }


    protected boolean setTableLine(String table, int state, Hashtable line, RuleStateItem item, Integer action, String terminal) {
        // set one action into a table row and detect conflicts
        Object o = line.get(terminal);
        if (o == null) {    // no conflict
            line.put(terminal, action);
        } else {    // conflict?
            if (o.equals(action) == false) {    // conflict!
                System.err.println("========================================================");
                System.err.println("WARNING: " + table + " state " + state + ", terminal " +
                        terminal + " is " +
                        displayAction(o) + " and was overwritten by action " +
                        displayAction(action));
                System.err.println("... from rule state: " + item);
                System.err.println("... current state:\n" + this);
                System.err.println("========================================================");
                return false;
            }
        }
        return true;
    }

    private String displayAction(Object action) {
        if (action.equals(ParserTables.SHIFT))
            return "SHIFT";
        return "REDUCE(" + action.toString() + ")";
    }


    public boolean equals(Object o) {
        //System.err.println("LRPrimitive.equals: \n"+this+"\n with \n"+o);
        LRPrimitive node = (LRPrimitive) o;

        if (node.kernels != kernels)
            return false;

        // look if all entries are in the other node
        for (Enumeration e = entries.elements(); e.hasMoreElements(); ) {
            RuleStateItem item = (RuleStateItem) e.nextElement();
            // kernel items have pointer not at start
            if (item.pointerPosition > 1 && node.entries.containsKey(item) == false)
                return false;
        }
        return true;
    }


    public int hashCode() {
        if (hashCache == null) {
            int result = 0;
            for (Enumeration e = entries.elements(); e.hasMoreElements(); )
                result ^= e.nextElement().hashCode();
            hashCache = new Integer(result);
        }
        return hashCache.intValue();
    }


    public String toString() {
        StringBuffer sb = new StringBuffer();
        // we want a sorted output of items, order by ruleIndex
        List list = new ArrayList(entries.size());
        for (Enumeration e = entries.elements(); e.hasMoreElements(); ) {
            RuleStateItem rsi = (RuleStateItem) e.nextElement();
            int index = -1;
            for (int i = 0; index == -1 && i < list.size(); i++) {
                RuleStateItem rsi1 = (RuleStateItem) list.get(i);
                if (rsi1.ruleIndex > rsi.ruleIndex || rsi1.ruleIndex == rsi.ruleIndex && rsi.pointerPosition > 1)
                    index = i;
            }
            if (index < 0)
                list.add(rsi);
            else
                list.add(index, rsi);
        }
        for (int i = 0; i < list.size(); i++) {
            sb.append("  ");
            sb.append(list.get(i).toString());
            sb.append("\n");
        }
        return sb.toString();
    }


    // Helper that hold two RuleStateItems
    private class Tuple {
        RuleStateItem o1, o2;

        Tuple(RuleStateItem o1, RuleStateItem o2) {
            this.o1 = o1;
            this.o2 = o2;
        }
    }


    protected class RuleStateItem {
        Rule rule;
        int pointerPosition = 1;
        int ruleIndex;
        int followNodeIndex = -1;
        protected Integer hashCache = null;


        public RuleStateItem(int ruleIndex, Rule rule) {
            this.rule = rule;
            this.ruleIndex = ruleIndex;
        }


        protected RuleStateItem(RuleStateItem orig) {
            this.rule = orig.rule;
            this.pointerPosition = orig.pointerPosition;
            this.ruleIndex = orig.ruleIndex;
        }


        protected RuleStateItem createRuleStateItem(RuleStateItem orig) {
            return new RuleStateItem(orig);
        }


        String getNonterminal() {
            return rule.getNonterminal();
        }


        RuleStateItem shift() {
            RuleStateItem clone = createRuleStateItem(this);
            clone.pointerPosition++;
            return clone;
        }


        String getPendingNonTerminal() {
            if (pointerPosition > rule.rightSize())
                return null;

            String symbol = getPendingSymbol();
            if (Token.isTerminal(symbol))
                return null;    // is a terminal

            return symbol;
        }


        String getPendingSymbol() {
            if (pointerPosition > rule.rightSize())
                return null;

            return rule.getRightSymbol(pointerPosition - 1);
        }


        public boolean equals(Object o) {
            RuleStateItem item = (RuleStateItem) o;
            return ruleIndex == item.ruleIndex && pointerPosition == item.pointerPosition;
        }


        public int hashCode() {
            if (hashCache == null)
                hashCache = new Integer(ruleIndex * 13 + pointerPosition);
            return hashCache.intValue();
        }


        public String toString() {
            StringBuffer sb = new StringBuffer("(Rule " + ruleIndex + ") " + getNonterminal() + " : ");
            int i = 0;
            for (; i < rule.rightSize(); i++) {
                if (i == pointerPosition - 1)
                    sb.append(".");
                sb.append(rule.getRightSymbol(i));
                sb.append(" ");
            }
            if (i == pointerPosition - 1)
                sb.append(".");
            if (followNodeIndex >= 0)
                sb.append(" -> State " + followNodeIndex);
            return sb.toString();
        }

    }

}