package parser.parsertables;

import parser.syntax.Syntax;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;


public class LRParserTables extends LRPrimitiveTables {

    public LRParserTables(Syntax syntax)
            throws ParserBuildException {
        super(syntax);
    }

    public LRParserTables(ArrayList<Hashtable<String, Integer>> parserTables,
                          ArrayList<Hashtable<String, Integer>> gotoTables, Syntax syntax)
            throws  ParserBuildException{
        super(parserTables, gotoTables, syntax);
    }


    protected LRSyntaxNode createStartNode(Nullable nullable, FirstSets firstSets) {
        return new LRSyntaxNode(nullable, firstSets);
    }


    protected void init() throws ParserBuildException {

        Nullable nullable = new Nullable(syntax, nonterminals);
        firstSets = new FirstSets(syntax, nullable, nonterminals);

        syntaxNodes = createStartNode(nullable, firstSets).build(syntax, syntaxNodes, new Hashtable());


        if (gotoTable == null) {
            System.out.println("GO-TO tables was found on local system. But it loooks like tables are empty, " +
                    "regenerating GO-TO tables.");
            gotoTable = generateGoto(syntaxNodes);
        }else{
            this.readedFromFile[1] = true;
            System.out.println("GO-TO tables was found on local system. Program will not generate" +
                    " any tables itself.");
        }

        if (parseTable == null) {
            System.out.println("Parse-Action tables was found on local system. But it loooks like tables are empty, " +
                    "regenerating Parse-Action tables.");
            parseTable = generateParseAction(syntaxNodes);
        }else{
            this.readedFromFile[0] = true;
            System.out.println("Parse-Action tables was found on local system. Program will not generate" +
                    " any tables itself.");
        }
    }
}