package parser.parsertables;

import parser.ParserTables;
import parser.Token;
import parser.syntax.Rule;
import parser.syntax.Syntax;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public abstract class AbstractParserTables implements ParserTables {

    protected Syntax syntax;
    public boolean readedFromFile[] ={
            false,
            false};
    protected List gotoTable;

    protected List parseTable;

    protected transient List symbols = new ArrayList();

    protected transient List terminals = new ArrayList();

    protected List terminalsWithoutEpsilon = new ArrayList();

    protected transient List nonterminals = new ArrayList();

    public transient static int CELLWIDTH = 0;

    protected AbstractParserTables() {

    }

    public Integer getGotoState(Integer currentState, String symbol) {
        Integer state = null;
        Map map = (Map) gotoTable.get(currentState);
        if (map != null)
            state = (Integer) map.get(symbol);
        return state == null ? ParserTables.ERROR : state;
    }

    public Integer getParseAction(Integer currentState, String terminal) {
        Integer action = null;
        Map map = (Map) parseTable.get(currentState);
        if (map != null)
            action = (Integer) map.get(terminal);
        return action == null ? ParserTables.ERROR : action;
    }

    public List getTerminals() {
        return terminalsWithoutEpsilon;
    }

    public Syntax getSyntax() {
        return syntax;
    }

    public Map getExpected(Integer state) {
        return (Map) parseTable.get(state);
    }

    public void dump(PrintStream out) {
        dumpSyntax(out);
        dumpTables(out);
    }

    public void dumpTables(PrintStream out) {
        dumpGoto(out);
        dumpParseAction(out);
    }

    public void dumpSyntax(PrintStream out) {
        for (int i = 0; i < syntax.size(); i++)
            out.println(dumpRule(syntax.getRule(i), i));
        out.println();
    }

    protected String dumpRule(Rule rule, int i) {
        StringBuilder sb = new StringBuilder("(Rule " + i + ")  " + rule.getNonterminal() + " : ");
        for (int j = 0; j < rule.rightSize(); j++)
            sb.append(rule.getRightSymbol(j)).append(" ");
        return sb.toString();
    }

    protected void dumpGoto(PrintStream out) {
        if (symbols.size() > 0)
            dumpTable("GOTO TABLE", symbols, gotoTable, out);
    }

    protected void dumpParseAction(PrintStream out) {
        if (terminals.size() > 0)
            dumpTable("PARSE-ACTION TABLE", terminals, parseTable, out);
    }


    protected void dumpTable(String title, List head, List table, PrintStream out) {
        out.println(title);
        out.println(dummy(title.length(), "="));

        // if no CELLWIDTH is set, estimate maximum cell width
        if (CELLWIDTH <= 0) {
            for (Iterator it = head.iterator(); it.hasNext(); ) {
                String s = (String) it.next();
                if (s.length() > CELLWIDTH && it.hasNext())    // not last
                    CELLWIDTH = s.length() + 1;
            }
        }

        // print table header
        dumpCell(" | ", CELLWIDTH, false, out);
        for (Iterator it = head.iterator(); it.hasNext(); ) {
            String s = (String) it.next();
            if (Token.isEpsilon(s))
                s = "<EOF>";
            dumpCell(s, CELLWIDTH, !it.hasNext(), out);
        }

        out.println();
        out.println(dummy(CELLWIDTH * (head.size() + 1), "_"));

        int i = 0;
        for (Iterator it = table.iterator(); it.hasNext(); i++) {
            Map h = (Map) it.next();

            dumpCell(Integer.toString(i) + " | ", CELLWIDTH, false, out);
            for (Iterator it2 = head.iterator(); it2.hasNext(); ) {
                String symbol = (String) it2.next();

                Integer intg = h != null ? (Integer) h.get(symbol) : null;
                String digit = intg == null ? "-" : intg.equals(ParserTables.SHIFT) ? "SH" : intg.equals(ParserTables.ACCEPT) ? "AC" : intg.toString();

                dumpCell(digit, CELLWIDTH, !it2.hasNext(), out);
            }
            out.println();
        }

        out.println();
    }

    private void dumpCell(String digit, int width, boolean isLast, PrintStream out) {
        StringBuilder tab = new StringBuilder(" ");
        for (int i = 0; i < width - 1 - digit.length(); i++)
            tab.append(' ');

        String s = tab + digit;
        if (!isLast && s.length() > width && width > 2)
            s = s.substring(0, width);

        out.print(s);
    }

    private String dummy(int width, String ch) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < width; i++)
            sb.append(ch);
        return sb.toString();
    }


    public void report(PrintStream out) {
        out.println("rules: " + syntax.size());
        out.println("terminals: " + terminals.size());
        out.println("nonterminals: " + nonterminals.size());
    }

}