package parser;


public class Token {

    public static final String EPSILON = "\"EoI\"";


    public static final char STRING_QUOTE = '"';


    public static final char CHAR_QUOTE = '\'';


    public static final char COMMAND_QUOTE = '`';


    public static final String UPTO = "..";


    public static final String BUTNOT = "-";


    public static final String TOKEN = "token";


    public static final String IGNORED = "ignored";


    public static final String ARTIFICIAL_NONTERMINAL_START_CHARACTER = "_";


    public final String symbol;

    public final Object text;

    public final Range range;


    public static class Address implements Comparable {

        public final int line;

        public final int column;

        public final int offset;

        public Address() {
            this(1, 0, 0);
        }

        public Address(int line, int column, int offset) {
            this.line = line;
            this.column = column;
            this.offset = offset;
        }

        public String toString() {
            return line + "/" + column;
        }

        public boolean equals(Object o) {
            return offset == ((Address) o).offset;
        }

        public int hashCode() {
            return offset;
        }

        public int compareTo(Object o) {
            return offset - ((Address) o).offset;
        }
    }


    public static class Range implements Comparable {

        public final Address start;

        public final Address end;

        public Range(Address start, Address end) {
            this.start = start != null ? start : new Address();
            this.end = end != null ? end : new Address();
        }

        public boolean equals(Object o) {
            return start.equals(((Range) o).start) && end.equals(((Range) o).end);
        }

        public int hashCode() {
            return start.hashCode() + end.hashCode();
        }

        public String toString() {
            return start + "-" + end;
        }

        public int compareTo(Object o) {
            return start.compareTo(((Range) o).start) + end.compareTo(((Range) o).end);
        }
    }


    public Token(String symbol, Object text, Range range) {
        this.symbol = symbol;
        this.text = text;
        this.range = range;
    }


    public static boolean isEpsilon(Token token) {
        return isEpsilon(token.symbol);
    }


    public static boolean isEpsilon(String symbol) {
        return symbol != null && symbol == EPSILON;
    }


    public static boolean isTerminal(String symbol) {
        char c = symbol.charAt(0);
        return
                c == STRING_QUOTE ||
                        c == CHAR_QUOTE ||
                        c == COMMAND_QUOTE ||
                        Character.isDigit(c) ||
                        Token.isEpsilon(symbol);
    }

}