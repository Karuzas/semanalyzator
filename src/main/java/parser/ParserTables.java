package parser;

import parser.syntax.Syntax;

import java.io.PrintStream;
import java.util.*;


public interface ParserTables {

    Integer ACCEPT = new Integer(0);


    Integer ERROR = new Integer(-1);


    Integer SHIFT = new Integer(-2);


    Integer getGotoState(Integer currentState, String symbol);


    Integer getParseAction(Integer currentState, String terminal);


    List getTerminals();


    Syntax getSyntax();

    void setParserTables(ArrayList<HashMap<String,Integer>> x);

    void dump(PrintStream out);

    void toFile(PrintStream out, PrintStream out2, Date lastModified);


    Map getExpected(Integer state);

}
