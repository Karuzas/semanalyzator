package parser.lexer;


public interface StrategyFactoryMethod {


    Strategy newStrategy();

}
