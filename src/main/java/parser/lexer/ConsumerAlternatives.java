package parser.lexer;

import parser.syntax.Rule;
import util.Equals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


class ConsumerAlternatives extends Consumer {
    private List<Consumer> alternates = new ArrayList<>(3);
    private StrategyFactoryMethod strategyFactoryMethod;
    private Strategy strategy;


    ConsumerAlternatives(Consumer alternateConsumer) {
        addAlternate(alternateConsumer);
    }


    public void addAlternate(Consumer alternateConsumer) {
        alternates.add(alternateConsumer);
    }


    public List getAlternatives() {
        return alternates;
    }


    boolean matchesRepeatableRule(Rule rule) {
        for (Consumer ac : alternates) {
            if (ac.matchesRepeatableRule(rule))
                return true;
        }
        return false;
    }


    public Character getStartCharacter() {
        Character c = null;

        for (int i = 0; i < alternates.size(); i++) {
            Consumer cc = alternates.get(i);
            Character c1 = cc.getStartCharacter();

            if (i == 0)
                c = c1;
            else if (!Equals.equals(c, c1))
                return null;
        }

        return c;
    }


    public int getStartVariance() {
        if (getStartCharacter() != null)
            return 1;

        int v = 0;
        for (Consumer alternate : alternates) v += alternate.getStartVariance();

        return v;
    }


    protected int getSomeLength(boolean exploreStartLength, List breakIndicator) {
        int max = 0;
        for (Consumer cc : alternates) {
            int len = cc.getSomeLength(exploreStartLength, breakIndicator);
            if (len > max)
                max = len;
        }
        return max;
    }


    protected ResultTree consumeInternal(InputText input)
            throws IOException {
        if (strategy == null) {    // sort alternatives by their start/fixed length
            strategy = strategyFactoryMethod != null ? strategyFactoryMethod.newStrategy() : new Strategy();

            for (Object alternate : alternates) {
                Consumer cc = (Consumer) alternate;
                strategy.addTokenConsumer(cc.rule.getNonterminal(), cc);
            }
        }

        Strategy.Item item = strategy.consume(input, input.peek(), null);
        if (item != null)
            return item.getResultTree();

        return null;
    }


    public boolean overlaps(Consumer cc) {
        for (Object alternate : alternates) {
            Consumer ac = (Consumer) alternate;
            if (ac.overlaps(cc))
                return true;
        }
        return false;
    }


    protected String toStringBase() {
        StringBuffer sb = new StringBuffer();
        listToString(alternates, sb, " | ", false);
        return sb.toString();
    }


    public void setStrategyFactoryMethod(StrategyFactoryMethod strategyFactoryMethod) {
        this.strategyFactoryMethod = strategyFactoryMethod;

        for (Object alternate : alternates) {
            Consumer c = (Consumer) alternate;
            c.setStrategyFactoryMethod(strategyFactoryMethod);
        }
    }

}