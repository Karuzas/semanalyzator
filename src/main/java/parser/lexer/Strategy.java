package parser.lexer;

import parser.Token;
import util.AggregatingHashtable;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;


public class Strategy {

    private boolean inited;
    private AggregatingHashtable itemsWithStartChar = new AggregatingHashtable();
    private List<Item> itemsWithoutStartChar = new ArrayList<>();
    private AggregatingHashtable competitiveGroups = new AggregatingHashtable();


    public void addIgnoringConsumer(String symbol, Consumer cc) {
        addConsumer(symbol, cc);
    }


    public void addTokenConsumer(String symbol, Consumer cc) {
        addConsumer(symbol, cc);
    }


    public boolean hasTerminal(String terminal) {
        for (Enumeration e = new ItemEnumerator(); e.hasMoreElements(); )
            if (((Item) e.nextElement()).getSymbol().equals(terminal))
                return true;
        return false;
    }

    private void addConsumer(String symbol, Consumer cc) {
        Item item = new Item(symbol, cc);
        Character c = item.consumer.getStartCharacter();
        if (c != null)
            itemsWithStartChar.put(c, item);
        else
            itemsWithoutStartChar.add(item);
    }


    private List getItemsWithStartCharacter(int c) {
        init();
        return (List) itemsWithStartChar.get(new Character((char) c));
    }


    private List<Item> getItemsWithoutStartCharacter() {
        init();
        return itemsWithoutStartChar;
    }


    // separate consumers with or without fixed start character, build hashtable for start characters
    private void init() {
        if (!inited) {
            inited = true;
            // sort items with start character
            for (Object o : itemsWithStartChar.entrySet()) {
                Map.Entry entry = (Map.Entry) o;
                Collections.sort((List) entry.getValue());
            }
            // sort items without start character
            Collections.sort(itemsWithoutStartChar);
        }
    }

    private List initCompetitors(Item candidate) {
        List competitors = (List) competitiveGroups.get(candidate);
        if (competitors != null)    // already inited
            if (competitors.get(0) instanceof Item)    // valid competitor list
                return competitors;
            else    // was dummy object
                return null;

        for (Enumeration e = new ItemEnumerator(); e.hasMoreElements(); ) {    // search a competitor among all items
            Item item = (Item) e.nextElement();
            if (item != candidate && item.consumer.overlaps(candidate.consumer)) {
                //System.err.println("Found competitive consumers: candidate = "+candidate.consumer+", competitor = "+item.consumer);
                competitiveGroups.put(candidate, item);
            }
        }

        competitors = (List) competitiveGroups.get(candidate);
        if (competitors == null)    // no competitors were found, mark candidate with dummy object
            competitiveGroups.put(candidate, (byte) 0);

        return competitors;
    }


    public Item consume(InputText input, int lookahead, Map expectedTokenSymbols)
            throws IOException {
        // try all scan-items by sort order, indexed ones first
        List[] allItems = new List[]{getItemsWithStartCharacter(lookahead), getItemsWithoutStartCharacter()};

        for (List items : allItems) {
            if (items != null) {
                for (Object item1 : items) {
                    Item item = (Item) item1;

                    if (expectedTokenSymbols == null || expectedTokenSymbols.get(item.getSymbol()) != null) {
                        int startMark = input.getMark();    // save start position for concurrent consumers
                        ResultTree result = item.consume(input);

                        if (result != null) {    // consumer succeeded
                            List competitors = initCompetitors(item);
                            if (competitors != null) {
                                int bestMark = input.getMark();
                                input.setMark(startMark);
                                item = checkCompetitiveGroups(item, input, competitors, bestMark, expectedTokenSymbols);    // returns item that scans longest
                            }
                            return item;
                        }
                    }
                }    // end for all items
            }    // end if item != null
        }    // end for both item groups

        if (expectedTokenSymbols != null)    // when this was a run with hints,
            return consume(input, lookahead, null);    // now try without hints

        return null;
    }


    // loop competitive group of passed Item (if existent), return given Item or an Item that scans longer
    private Item checkCompetitiveGroups(Item item, InputText input, List competitors, int bestMark, Map expectedTokenSymbols)
            throws IOException {
        int max = bestMark - input.getMark();

        for (Object competitor1 : competitors) {
            Item competitor = (Item) competitor1;

            // let take part only if no expected symbols or is in expected symbols
            if (expectedTokenSymbols != null && expectedTokenSymbols.get(competitor.getSymbol()) == null)
                continue;

            int mark = input.getMark();    // memorize current input mark

            ResultTree r = competitor.consume(input);    // consume
            int len = input.getMark() - mark;

            if (r != null && len > max) {    // scanned longer
                bestMark = input.getMark();
                max = len;
                item = competitor;
            }

            input.setMark(mark);    // reset for next candidate
        }

        input.setMark(bestMark);    // set mark forward to best scan result

        return item;
    }


    public String toString() {
        init();
        StringBuilder sb = new StringBuilder();
        sb.append("  Indexed list is: ").append(itemsWithStartChar.size()).append("\n");
        for (Map.Entry entry : (Iterable<Map.Entry>) itemsWithStartChar.entrySet()) {
            sb.append("    ").append(entry.getKey()).append("	->	").append(entry.getValue()).append("\n");
        }
        sb.append("  Sorted unindexed list is: ").append(itemsWithoutStartChar.size()).append("\n");
        for (Item anItemsWithoutStartChar : itemsWithoutStartChar) {
            sb.append("    ").append(anItemsWithoutStartChar).append("\n");
        }
        return sb.toString();
    }


    public static class Item implements
            Comparable,
            Serializable {
        private String symbol;
        private Consumer consumer;
        private transient ResultTree result;

        public Item(String symbol, Consumer consumer) {
            if (consumer == null)
                throw new IllegalArgumentException("Got no character consumer for symbol " + symbol);
            if (symbol == null)
                throw new IllegalArgumentException("Got no symbol for consumer " + consumer);

            this.symbol = symbol;
            this.consumer = consumer;
        }


        public ResultTree consume(InputText input)
                throws IOException {
            return result = consumer.consume(input);
        }


        public String getSymbol() {
            return symbol;
        }


        public String getTokenIdentifier() {
            return Token.isTerminal(symbol) ? symbol : Token.COMMAND_QUOTE + symbol + Token.COMMAND_QUOTE;
        }

        public ResultTree getResultTree() {    // this is for ConsumerAlternatives
            return result;
        }

        public String toString() {
            return "{" + symbol + "} " + consumer.toString();
        }


        public int compareTo(Object o) {
            Consumer cc1 = consumer;
            Consumer cc2 = ((Item) o).consumer;
            int i;
            if ((i = cc1.compareTo(cc2)) != 0)
                return i;
            return Token.isTerminal(symbol) ? -1 : 1;
        }


        public boolean equals(Object o) {
            return ((Item) o).consumer == consumer;
        }


        public int hashCode() {
            return consumer.hashCode();
        }
    }


    // enumeration over all strategy items (toplevel consumers)
    private class ItemEnumerator implements Enumeration {
        private Iterator<Map.Entry> it;
        private Iterator it1;
        private Iterator<Item> it2;

        public ItemEnumerator() {
            it = itemsWithStartChar.entrySet().iterator();
            it1 = it.hasNext() ? ((List) (it.next()).getValue()).iterator() : null;
            it2 = itemsWithoutStartChar.iterator();
        }

        public boolean hasMoreElements() {
            return it.hasNext() || it1 != null && it1.hasNext() || it2.hasNext();
        }

        public Object nextElement() {
            if (it1 != null)
                if (it1.hasNext())
                    return it1.next();
                else if (it.hasNext())
                    return (it1 = ((List) (it.next()).getValue()).iterator()).next();

            if (it2.hasNext())
                return it2.next();

            throw new IllegalStateException("Do not call nextElement() when hasMoreElements() returned false!");
        }
    }

}
