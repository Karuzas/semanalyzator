package parser.lexer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


class InputText extends Input {
    private int[] line;    // current line buffer
    private int[] prevLine;    // previous line buffer
    private int prevLength;
    private boolean wasCr = false;    // flag if '\r' occured
    private int column;    // line position
    private List<Integer> lineLengths = new ArrayList<Integer>();    // list of Integer containing line lengths including newline sequence
    private int[] scanPoint;


    InputText(Object input)
            throws IOException {
        super(input);
    }


    protected int convertInput(int i) {
        if (i == '\r') {
            wasCr = true;
            newLine();
        } else {
            if (i == '\n') {
                if (!wasCr)    // on UNIX
                    newLine();
                else    // on WINDOWS, ignore \n after \r, but adjust previous line length
                    lineLengths.set(
                            lineLengths.size() - 1,
                            lineLengths.get(lineLengths.size() - 1) + 1
                    );
            } else {
                storeRead(i);
            }
            wasCr = false;
        }

        scanPoint = null;    // force new scan point calculation
        return i;
    }

    private void newLine() {
        lineLengths.add(column + 1);    // line length plus one for the newline, will be adjusted when \r\n

        prevLength = column;
        column = 0;    // reset current line offset

        if (line == null)
            line = new int[64];

        if (prevLine == null || prevLine.length < line.length)
            prevLine = new int[Math.max(line.length, 64)];

        System.arraycopy(line, 0, prevLine, 0, prevLength);
    }

    private void storeRead(int i) {
        if (line == null) {    // allocate buffer
            line = new int[64];
        } else if (column == line.length) {    // reallocate line buffer
            int[] old = line;
            line = new int[old.length * 2];
            System.arraycopy(old, 0, line, 0, old.length);
        }

        line[column] = i;
        column++;
    }


    public int getReadLine() {
        return lineLengths.size() + 1;
    }


    public int getReadColumn() {
        return column;
    }


    public int getScanLine() {
        return calculateScanPoint()[0];
    }


    public int getScanColumn() {
        return calculateScanPoint()[1];
    }

    private int[] calculateScanPoint() {    // returns line/column
        if (scanPoint != null)    // use buffered scan pointer when possible, as line/column requests always are coupled
            return scanPoint;

        int diff = getReadColumn() - getUnreadLength();
        if (diff >= 0)
            return scanPoint = new int[]{getReadLine(), diff};

        for (int i = lineLengths.size() - 1; i >= 0; i--) {    // loop back all lines
            int len = lineLengths.get(i);
            diff += len;
            if (diff >= 0)
                return scanPoint = new int[]{i + 1, diff};
        }
        throw new IllegalStateException("Something went wrong when calculating scan point: " + diff);
    }


    public int read() throws IOException {
        scanPoint = null;
        return super.read();
    }


    public void setMark(int mark) {
        scanPoint = null;
        super.setMark(mark);
    }


    public String getLine() {
        return createLineString(line, column);
    }


    public String getPreviousLine() {
        return createLineString(prevLine, prevLength);
    }


    public String getUnreadText() {
        int[] buf = getUnreadBuffer();
        return createLineString(buf, buf.length);
    }

    private String createLineString(int[] line, int end) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < end; i++)
            sb.append((char) line[i]);
        return sb.toString();
    }

}