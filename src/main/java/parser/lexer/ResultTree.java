package parser.lexer;

import parser.Token;
import parser.syntax.Rule;

import java.util.ArrayList;
import java.util.List;


public class ResultTree {

    private Rule rule;
    private List<Object> sequenceList;
    private Token.Range range;
    private StringBuffer buffer;
    private String stringResult;

    ResultTree(Rule rule) {
        this.rule = rule;
    }


    public Token.Range getRange() {
        return range;
    }

    void setRange(Token.Range range) {
        this.range = range;
    }


    public boolean hasText() {
        for (int i = 0; sequenceList != null && i < sequenceList.size(); i++) {
            Object o = sequenceList.get(i);
            if (!(o instanceof ResultTree))
                return true;    // StringBuffer was allocated with minimum one char
            else if (((ResultTree) o).hasText())
                return true;
        }
        return false;
    }


    public String toString() {
        if (stringResult == null) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; sequenceList != null && i < sequenceList.size(); i++) {
                Object o = sequenceList.get(i);
                if (o instanceof ResultTree)
                    sb.append(o.toString());
                else
                    sb.append(o.toString());

            }
            stringResult = sb.toString();
        }
        return stringResult;
    }


    public Rule getRule() {
        return rule;
    }


    public Object getChild(int i) {
        Object o = sequenceList.get(i);
        return o instanceof StringBuffer ? o.toString() : o;    // avoid StringBuffer.append calls
    }


    public int getChildCount() {
        return sequenceList != null ? sequenceList.size() : 0;
    }

    ResultTree append(char c) {
        ensureStringBuffer(null).append(c);
        return this;
    }

    ResultTree append(String s) {
        ensureStringBuffer(s);
        return this;
    }

    ResultTree addChild(ResultTree r) {
        if (r.hasText()) {
            ensureSequenceList().add(r);
            buffer = null;    // force new list element
        }
        return this;
    }

    private List<Object> ensureSequenceList() {
        if (sequenceList == null)
            sequenceList = new ArrayList<>(rule != null ? rule.rightSize() : 1);
        return sequenceList;
    }

    private StringBuffer ensureStringBuffer(String toStore) {
        if (buffer == null) {
            if (toStore == null)
                buffer = new StringBuffer(8);
            else
                buffer = new StringBuffer(toStore);
            ensureSequenceList().add(buffer);
        } else if (toStore != null) {
            buffer.append(toStore);
        }
        return buffer;
    }

}
