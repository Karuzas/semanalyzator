package parser.lexer;

import parser.syntax.Rule;

import java.util.Set;


public interface LexerSemantic {


    void ruleEvaluated(Rule rule, ResultTree resultTree);


    Set getWantedNonterminals();


    Set getIgnoredNonterminals();

}
