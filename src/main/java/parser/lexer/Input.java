package parser.lexer;

import java.io.*;


class Input {
    public static final int EOF = -1;
    private InputStream inputStream;
    private Reader reader;
    private int[] buffer;
    private int readPos, readLen;
    private int readOffset;
    private boolean eof = false;
    private boolean buffering = false;


    Input(Object input)
            throws IOException {
        if (input instanceof InputStream)
            this.inputStream = input instanceof BufferedInputStream ? (InputStream) input : new BufferedInputStream((InputStream) input);
        else if (input instanceof Reader)
            this.reader = input instanceof BufferedReader ? (Reader) input : new BufferedReader((Reader) input);
        else if (input instanceof File)
            this.reader = new BufferedReader(new FileReader((File) input));
        else if (input instanceof StringBuffer || input instanceof String)
            this.reader = new StringReader(input.toString());
        else
            throw new IllegalArgumentException("Unknown input object: " + (input != null ? input.getClass().getName() : "null"));
    }


    public int read()
            throws IOException {
        int i;

        if (readLen > readPos) {    // use buffer if buffer is not at end
            i = buffer[readPos];
            readPos++;
            return i;
        }

        if (eof)
            return EOF;

        // read one character from input
        i = (reader != null) ? reader.read() : inputStream.read();

        // recognize end of input
        if (i == -1) {
            eof = true;

            try {
                if (reader != null)
                    reader.close();
                else
                    inputStream.close();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        } else {
            readOffset++;
            convertInput(i);    // input hook for subclasses

            if (buffering)    // store character if in buffering state
                storeRead(i);
            else
                readPos = readLen = 0;
        }

        return i;
    }


    public int peek()
            throws IOException {
        int mark = getMark();
        int c = read();
        setMark(mark);
        return c;
    }


    protected int convertInput(int i) {
        return i;
    }


    public int getScanOffset() {
        return getReadOffset() - getUnreadLength();
    }


    public int getReadOffset() {
        return readOffset;
    }


    public int getMark() {
        buffering = true;
        return readPos;
    }


    public void setMark(int mark) {
        readPos = mark;
    }


    public void resolveBuffer() {
        buffering = false;

        if (readLen > readPos) {
            if (readPos > 0) {    // copy unread buffer to bottom
                int diff = getUnreadLength();
                System.arraycopy(buffer, readPos, buffer, 0, diff);
                readLen = diff;
                readPos = 0;
            }
        } else {
            readPos = readLen = 0;
        }
    }


    // store the int to buffer
    private void storeRead(int i) {
        if (buffer == null)    // allocate buffer
            buffer = new int[128];

        if (readPos == buffer.length) {    // reallocate buffer as it is too small
            //System.err.println("enlarging lexer buffer from "+buffer.length);
            int[] old = buffer;
            // the buffer must not be as long as the input, it just serves as lookahead buffer.
            buffer = new int[old.length * 2];
            System.arraycopy(old, 0, buffer, 0, old.length);
        }

        if (readPos != readLen)
            throw new IllegalStateException("Can not read to buffer when it was not read empty!");

        buffer[readPos] = i;
        readPos++;
        readLen++;
    }


    public int[] getUnreadBuffer() {
        int diff;
        if (buffer == null || (diff = getUnreadLength()) <= 0)
            return new int[0];

        int[] ret = new int[diff];
        System.arraycopy(buffer, readPos, ret, 0, diff);

        return ret;
    }


    protected int getUnreadLength() {
        return readLen - readPos;
    }

}