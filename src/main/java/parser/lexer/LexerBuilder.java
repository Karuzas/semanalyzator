package parser.lexer;

import parser.Lexer;
import parser.Token;
import parser.syntax.Rule;
import parser.syntax.Syntax;
import parser.syntax.SyntaxException;
import parser.syntax.builder.SyntaxSeparation;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class LexerBuilder {
    protected Map charConsumers;
    protected List ignoredSymbols;
    public static boolean DEBUG;    // defaults to false


    public LexerBuilder(Syntax lexerSyntax, List ignoredSymbols)
            throws LexerException, SyntaxException {
        this.ignoredSymbols = ignoredSymbols;
        build(lexerSyntax);
    }


    public Lexer getLexer() {
        return new LexerImpl(ignoredSymbols, charConsumers);
    }


    public Lexer getLexer(Object input)
            throws IOException {
        Lexer lexer = getLexer();
        lexer.setInput(input);
        return lexer;
    }


    private void build(Syntax lexerSyntax)
            throws LexerException, SyntaxException {
        SyntaxSeparation.IntArray deleteIndexes = new SyntaxSeparation.IntArray(lexerSyntax.size());
        if (DEBUG)
            System.err.println("Processing lexer rules: \n" + lexerSyntax);

        // resolve scanner rules to Consumers and put it into a hashtable
        this.charConsumers = new Hashtable(lexerSyntax.size());
        for (int i = 0; i < lexerSyntax.size(); i++)
            translateLexerRule(lexerSyntax.getRule(i), i, deleteIndexes);
        deleteIndexes.removeIndexesFrom(lexerSyntax);

        // check for unresolved repeatable and nullable rules and delete them from lexer syntax
        for (int i = 0; i < lexerSyntax.size(); i++) {
            Rule rule = lexerSyntax.getRule(i);
            String nonterm = rule.getNonterminal();
            if (checkNullableRule(nonterm, rule, i, deleteIndexes) == false)
                if (checkRepeatableRule(nonterm, rule, i, deleteIndexes) == false)
                    throw new LexerException("Found no character consumer for nullable or repeatable rule " + rule);
        }
        deleteIndexes.removeIndexesFrom(lexerSyntax);

        if (lexerSyntax.size() > 0) {    // not all rules have been resolved to character consumers
            throw new LexerException("Could not process rules in lexer syntax: " + lexerSyntax);
        }

        // resolve all symbolic consumer references after all consumers have been created
        Map done = new Hashtable();    // beware of recursion
        for (Iterator it = charConsumers.entrySet().iterator(); it.hasNext(); ) {
            Consumer cc = (Consumer) ((Map.Entry) it.next()).getValue();
            cc.resolveConsumerReferences(charConsumers, done);
        }
    }


    private void translateLexerRule(Rule rule, int index, SyntaxSeparation.IntArray deleteIndexes)
            throws LexerException {
        String nonterm = rule.getNonterminal();
        if (rule.rightSize() <= 0 || rule.getRightSymbol(0).equals(nonterm))    // nullable rules and left recursive rules will be resolved later
            return;

        int CONCATENATION = 0, SET = 1, SUBTRACTION = 2;
        int state = CONCATENATION;
        boolean intersectionHappened = false;
        Consumer consumer = new Consumer(rule);    // master consumer
        Consumer currentConsumer = new Consumer();
        Consumer setConsumer = currentConsumer;    // will host set definitions
        consumer.append(currentConsumer);    // will be resolved when trivial

        for (int i = 0; i < rule.rightSize(); i++) {    // loop all symbols on right side
            String sym = rule.getRightSymbol(i);

            if (sym.equals(Token.BUTNOT)) {
                if (i == 0 || state != CONCATENATION)
                    throw new LexerException("Missing symbol to subtract from: " + rule);
                state = SUBTRACTION;
            } else if (sym.equals(Token.UPTO)) {
                if (i == 0 || state != CONCATENATION)
                    throw new LexerException("Missing lower limit of set: " + rule);
                state = SET;
            } else {
                String convertedSym = convertSymbol(sym);    // remove quotes or convert number to char
                boolean isNonterm = convertedSym.equals(sym);
                if (isNonterm && state == SET)
                    throw new LexerException("Can not append nonterminal to set: " + rule);

                boolean setWillHappen = rule.rightSize() > i + 1 && rule.getRightSymbol(i + 1).equals(Token.UPTO);    // next symbol will be ".."

                if (state == SET) {
                    setConsumer.appendSet(convertedSym);
                    setConsumer = currentConsumer;    // reset if intersection happened
                } else if (state == SUBTRACTION) {
                    intersectionHappened = true;
                    if (isNonterm)
                        if (setWillHappen)
                            throw new LexerException("Nonterminal can not open set after subtraction: " + rule);
                        else
                            currentConsumer.subtract(new Consumer.Reference(sym));
                    else if (setWillHappen)
                        currentConsumer.subtract(setConsumer = new Consumer(convertedSym));
                    else
                        currentConsumer.subtract(new Consumer(convertedSym));
                } else if (state == CONCATENATION) {
                    if (intersectionHappened) {    // start new consumer
                        intersectionHappened = false;
                        currentConsumer = new Consumer();
                        consumer.append(currentConsumer);
                    }

                    if (isNonterm)
                        if (setWillHappen)
                            throw new LexerException("Nonterminal can not open set in concatenation: " + rule);
                        else
                            currentConsumer.append(new Consumer.Reference(sym));
                    else
                        currentConsumer.append(convertedSym);    // a following set will be recognized by consumer
                }

                state = CONCATENATION;    // reset to normal state

            }    // end switch current symbol
        }    // end for right side of rule

        putCharConsumer(nonterm, consumer.optimize());
        deleteIndexes.add(index);
    }


    private void putCharConsumer(String key, Consumer consumer) {
        //System.err.println("putting character consumer for "+key);
        Object o = charConsumers.get(key);    // test if existing

        if (o == null) {    // not in list
            charConsumers.put(key, consumer);
        } else {
            ConsumerAlternatives ca;

            if (o instanceof ConsumerAlternatives == false) {
                ca = new ConsumerAlternatives((Consumer) o);
                charConsumers.put(key, ca);    // replace consumer
            } else {
                ca = (ConsumerAlternatives) o;
            }

            ca.addAlternate(consumer);    // add a new alternative
        }
    }


    private boolean checkNullableRule(String nonterm, Rule rule, int index, SyntaxSeparation.IntArray deleteIndexes) {
        if (rule.rightSize() <= 0) {
            Object o = charConsumers.get(nonterm);
            ((Consumer) o).setNullable();
            deleteIndexes.add(index);
            return true;    // do not explore empty rule, return "found nullable"
        }
        return false;
    }


    private boolean checkRepeatableRule(String nonterm, Rule rule, int index, SyntaxSeparation.IntArray deleteIndexes) {

        if (rule.rightSize() >= 2 && rule.getRightSymbol(0).equals(nonterm)) {    // left recursive
            Consumer cc = (Consumer) charConsumers.get(nonterm);
            if (cc.matchesRepeatableRule(rule)) {    // check if rest on the right is same
                cc.setRepeatable();
                deleteIndexes.add(index);
                return true;
            }
        }
        return false;
    }

    private String convertSymbol(String sym) {
        if (sym.charAt(0) == '\'' || sym.charAt(0) == '"') {
            String s = sym.substring(1, sym.length() - 1);
            if (s.length() <= 0)
                throw new IllegalArgumentException("Empty character or string definition: " + sym);

            StringBuffer sb = new StringBuffer(s.length());    // convert escape sequences to their real meaning
            for (int i = 0; i < s.length(); i++) {
                char c = s.charAt(i);
                if (c == '\\') {
                    char c1 = s.length() > i + 1 ? s.charAt(i + 1) : 0;
                    switch (c1) {
                        case 'n':
                            sb.append('\n');
                            i++;
                            break;
                        case 'r':
                            sb.append('\r');
                            i++;
                            break;
                        case 't':
                            sb.append('\t');
                            i++;
                            break;
                        case 'f':
                            sb.append('\f');
                            i++;
                            break;
                        case 'b':
                            sb.append('\b');
                            i++;
                            break;
                        case '\'':
                            sb.append('\'');
                            i++;
                            break;
                        case '"':
                            sb.append('"');
                            i++;
                            break;
                        case '\\':
                            sb.append('\\');
                            i++;
                            break;
                        default:
                            sb.append(c);
                            break;
                    }
                } else {
                    sb.append(c);
                }
            }
            return sb.toString();
        } else {    // must be starting with digit or be a nonterminal
            char c;
            if (sym.startsWith("0x") || sym.startsWith("0X"))    // hexadecimal number
                c = (char) Integer.valueOf(sym.substring(2), 16).intValue();
            else if (sym.startsWith("0"))    // octal number
                c = (char) Integer.valueOf(sym.substring(1), 8).intValue();
            else if (Character.isDigit(sym.charAt(0)))
                c = (char) Integer.valueOf(sym).intValue();    // will throw NumberFormatException when not number
            else
                return sym;    // is a nonterminal

            return new String(new char[]{c});
        }
    }

}
