package parser.lexer;

import parser.Lexer;
import parser.Token;

import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class LexerImpl implements Lexer, StrategyFactoryMethod {

    protected Strategy strategy;
    private List<String> ignoredSymbols;
    private Map<String, Consumer> charConsumers;
    private transient InputText input;
    private List<TokenListener> listeners;
    private transient boolean debug;


    public LexerImpl(List ignoredSymbols, Map charConsumers) {
        setConsumers(ignoredSymbols, charConsumers);
    }


    public void addTokenListener(Lexer.TokenListener tokenListener) {
        if (listeners == null)
            listeners = new ArrayList<>(1);
        listeners.add(tokenListener);
    }


    public void removeTokenListener(Lexer.TokenListener tokenListener) {
        if (listeners != null)
            listeners.remove(tokenListener);
    }


    private void setConsumers(List<String> ignoredSymbols, Map<String, Consumer> charConsumers) {
        this.charConsumers = charConsumers;    // store for check at setTerminals()
        this.ignoredSymbols = ignoredSymbols;    // need to know which token should be ignored

        for (String sym : ignoredSymbols) {
            ensureStrategy().addIgnoringConsumer(sym, charConsumers.get(sym));
        }

        for (Consumer consumer : charConsumers.values()) {
            consumer.setStrategyFactoryMethod(this);
        }
    }

    private Strategy ensureStrategy() {
        if (strategy == null)
            strategy = newStrategy();
        return strategy;
    }


    public Strategy newStrategy() {
        return new Strategy();
    }


    public void setInput(Object text)
            throws IOException {
        input = new InputText(text);
    }


    public void setTerminals(List terminals) {
        for (Object terminal : terminals) {
            String symbol = (String) terminal;

            // check if it is a terminal as this is a public call
            if (symbol.length() <= 2 || !Token.isTerminal(symbol))
                throw new IllegalArgumentException("Terminals must be enclosed within quotes: " + symbol);

            String text = symbol.substring(1, symbol.length() - 1);    // remove quotes

            if (!ensureStrategy().hasTerminal(symbol)) {    // could have been called for second time
                if (symbol.charAt(0) == Token.COMMAND_QUOTE) {    // is a scan terminal covered by a Consumer
                    Consumer cc = charConsumers.get(text);
                    if (cc == null)
                        throw new IllegalArgumentException("Lexer token is not among character consumers: " + text);
                    else
                        ensureStrategy().addTokenConsumer(symbol, cc);
                } else {
                    ensureStrategy().addTokenConsumer(symbol, new Consumer(text));
                }
            }
        }    // end for

        if (debug)
            System.err.println("StrategyList is:\n" + strategy);
    }


    public void clear() {
    }


    public Token getNextToken(LexerSemantic lexerSemantic)
            throws IOException {
        return getNextToken(lexerSemantic, null);
    }


    public Token getNextToken(Map expectedTokenSymbols)
            throws IOException {
        return getNextToken(null, expectedTokenSymbols);
    }

    private Token getNextToken(LexerSemantic lexerSemantic, Map expectedTokenSymbols)
            throws IOException {
        if (input == null)
            throw new IllegalStateException("Lexer has no input, call setInput(...).");

        Token.Address start = new Token.Address(input.getScanLine(), input.getScanColumn(), input.getScanOffset());
        int c = input.peek();    // read lookahead
        if (c == Input.EOF)
            return createToken(Token.EPSILON, null, new Token.Range(start, start));

        // not EOF, there must be a lexer item or error
        Strategy.Item item = getNextLexerItem(expectedTokenSymbols, c);

        if (item != null) {    // successful scan
            if (ignoredSymbols != null && ignoredSymbols.indexOf(item.getSymbol()) >= 0) {
                if (listeners != null && listeners.size() > 0)    // creating a token takes time, do it only when listeners are present
                    fireTokenReceived(createToken(item.getTokenIdentifier(), item.getResultTree(), lexerSemantic), true);
                return getNextToken(expectedTokenSymbols);
            } else {
                Token token = createToken(item.getTokenIdentifier(), item.getResultTree(), lexerSemantic);
                fireTokenReceived(token, false);
                return token;
            }
        }

        // error state, return an error Token with null symbol
        Token.Address end = new Token.Address(input.getReadLine(), input.getReadColumn(), input.getScanOffset());
        return createToken(null, input.getUnreadText(), new Token.Range(start, end));
    }

    // strategic scan of next item
    private Strategy.Item getNextLexerItem(Map expectedTokenSymbols, int lookahead)
            throws IOException {
        if (strategy == null)
            throw new IllegalStateException("Lexer has no terminals, call setTerminals(syntaxSeparation.getTokenSymbols()).");

        Strategy.Item item = strategy.consume(input, lookahead, expectedTokenSymbols);

        if (item != null)
            input.resolveBuffer();    // forget old contents

        return item;
    }

    // calls the token listeners with scanned token
    private void fireTokenReceived(Token token, boolean ignored) {
        for (int i = 0; listeners != null && i < listeners.size(); i++)
            (listeners.get(i)).tokenReceived(token, ignored);
    }


    protected Token createToken(String tokenIdentifier, ResultTree result, LexerSemantic lexerSemantic) {
        if (lexerSemantic != null)
            loopResultTree(result, lexerSemantic);
        return createToken(tokenIdentifier, result.toString(), result.getRange());    // analyzeSemantics() takes time as it builds the token text
    }


    protected Token createToken(String tokenIdentifier, String text, Token.Range range) {
        return new Token(tokenIdentifier, text, range);
    }


    public boolean lex(LexerSemantic lexerSemantic)
            throws IOException {
        int c = input.peek();
        boolean eof = (c == Input.EOF);
        boolean error = eof;

        if (error == false) {
            Strategy.Item item = getNextLexerItem(null, c);
            error = (item == null || item.getTokenIdentifier() == null);

            if (error == false && lexerSemantic != null)
                loopResultTree(item.getResultTree(), lexerSemantic);

            c = input.peek();
            eof = (c == Input.EOF);
            error = (eof == false);
        }

        if (error) {
            dump(System.err);
            System.err.println("Could not process character '" + (char) c + "' (int " + c + "), at line/column " + input.getScanLine() + "/" + input.getScanColumn() + ", at offset " + input.getScanOffset());
        }

        return error == false;
    }


    protected void loopResultTree(ResultTree result, LexerSemantic lexerSemantic) {
        Set wantedNonterminals = lexerSemantic.getWantedNonterminals();
        Set ignoredNonterminals = lexerSemantic.getIgnoredNonterminals();
        String nonterminal = result.getRule().getNonterminal();

        if (!nonterminal.startsWith(Token.ARTIFICIAL_NONTERMINAL_START_CHARACTER) &&
                (wantedNonterminals == null || wantedNonterminals.contains(nonterminal)) &&
                (ignoredNonterminals == null || !ignoredNonterminals.contains(nonterminal))) {
            lexerSemantic.ruleEvaluated(result.getRule(), result);
        }

        for (int i = 0; i < result.getChildCount(); i++) {
            Object child = result.getChild(i);
            if (child instanceof ResultTree)
                loopResultTree((ResultTree) child, lexerSemantic);
        }
    }


    // debug methods


    public void setDebug(boolean debug) {
        this.debug = debug;
    }


    public String getLineText() {
        return input.getLine();
    }


    public void dump(PrintStream out) {
        int lineNr = input.getReadLine();
        String line = getLineText();

        if (lineNr > 1) {
            String prevLine = input.getPreviousLine();
            out.print((lineNr - 1) + ":\t");
            out.println(prevLine);
        }

        out.print(lineNr + ":\t");
        out.println(line);

        int nrLen = Integer.toString(lineNr).length();
        for (int i = 0; i < nrLen; i++)
            out.print(" ");

        out.print("\t");

        int errPos = input.getReadColumn();

        for (int i = 0; i < errPos && i < line.length(); i++)
            if (line.charAt(i) == '\t')
                out.print("\t");
            else
                out.print(" ");

        out.println("^");
    }

}
