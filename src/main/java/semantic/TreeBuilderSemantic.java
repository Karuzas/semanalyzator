package semantic;

import com.google.common.collect.Lists;
import parser.Token;
import parser.syntax.Rule;

import java.util.List;


public class TreeBuilderSemantic implements Semantic {

    public Object doSemantic(Rule rule, List<Object> inputTokens, List<Token.Range> ranges) {
        return new Node(rule, inputTokens, ranges);
    }

    public static class Node {
        private Rule rule;
        private List<Object> inputTokens;
        private List<Token.Range> ranges;

        public Node(Rule rule, List<Object> inputTokens, List<Token.Range> ranges) {
            this.rule = rule;
            this.inputTokens = inputTokens;
            this.ranges = ranges;
        }


        public Rule getRule() {
            return rule;
        }


        public List getInputTokens() {
            return inputTokens;
        }


        public List getRanges() {
            return ranges;
        }


        public String analyzeSemantics(int i) {
            return getRule().getNonterminal() + "@" + getRule().rightSize();
        }


        public void analyzeSemantics(int indent,
                                     List<String> funcId,
                                     List<String> identifiers,
                                     List<Variable> variables,
                                     List<Variable> globalVariables,
                                     List<Function> functions) {

            if (rule.getNonterminal().equals("method_invocation")) {
                String functionName = checkIsDeclaredFunction(functions, (Node) getInputTokens().get(0));
                if (getInputTokens().size() == 4) {
                    checkForParameters(functions, functionName, (Node) getInputTokens().get(2));
                }
            }

            if (rule.getNonterminal().equals("declarations_variable")) {
                safeVariable(functions);
                globalVariables.add(safeGlobalVariable());
            }

            if (rule.getNonterminal().equals("assignment")) {
                checkIsDeclared(globalVariables);
            }

            if (rule.getNonterminal().equals("function_header")) {
                functions.add(getFunction());
            }

            if (rule.getNonterminal().equals("declarations_function")) {
                analyzeFunctionIdentifiers(funcId);
            }

            if (rule.getNonterminal().equals("declarations_variable")) {
                analyzeIdentifiers(identifiers, funcId);
            }

            if (rule.getNonterminal().equals("block")) {
                List<String> id = Lists.newArrayList();
                List<Variable> vars = Lists.newArrayList();
                analyzeBlock(indent, funcId, id, vars, globalVariables, functions);
            } else {
                for (int i = 0; i < getInputTokens().size(); i++) {
                    Object o = getInputTokens().get(i);

                    if (o instanceof Node) {
                        ((Node) o).analyzeSemantics(indent + 1, funcId, identifiers, variables, globalVariables, functions);
                    }
                }
            }
        }

        private void checkForParameters(List<Function> functions, String functionName, Node node) {
            node = (Node) node.getInputTokens().get(0);
            int amount = 1;
            while (node.getRule().getSymbols().contains("RESERVED_COMMA")) {
                amount++;
                node = (Node) node.getInputTokens().get(0);
            }
            for (Function function : functions) {
                if (function.getName().equals(functionName)) {
                    if (function.getParameters().size() != amount) {
                        System.out.println("Error. wrong parameters on function " + functionName + " call." );
                    }
                }
            }
        }

        private String checkIsDeclaredFunction(List<Function> functions, Node node) {
            node = (Node) node.getInputTokens().get(0);
            String name = (String) node.getInputTokens().get(0);
            boolean contains = false;
            for (Function function : functions) {
                if (function.getName().equals(name)) {
                    contains = true;
                }
            }
            if (!contains) {
                System.out.println("Error. Not declared function!" + name + node.getRanges());
            }
            return name;
        }

        private Function getFunction() {
            return new Function(getFunctionName(), getFunctionType(), getFunctionParameters());
        }

        private void analyzeFunctionIdentifiers(List<String> funcId) {

            Node node = (Node) getInputTokens().get(0);
            node = (Node) node.getInputTokens().get(2);

            if (node.getRule().getSymbols().contains("`identifier`")) {
                String identifier = node.getInputTokens().get(0).toString();
                if (funcId.contains(identifier)) {
                    System.out.println("Error: already used identifier:" + identifier + "(" + getRanges() + ")");
                } else {
                    funcId.add(identifier);
                }
            }
        }

        private void checkIsDeclared(List<Variable> globalVariables) {
            Node node = (Node) getInputTokens().get(0);
            for (int i = 0; i < 2; i++) {
                node = (Node) node.getInputTokens().get(0);
            }
            boolean contains = false;
            for (Variable globalVariable : globalVariables) {
                String replace = globalVariable.getName().replace("[", "");
                String replaced = replace.replace("]", "");
                if (replaced.equals(node.getInputTokens().get(0).toString())) {
                    contains = true;
                }
            }
            if (!contains) {
                System.out.println("Error Undeclared variable" + node.getInputTokens() + node.getRanges());
            }
        }

        ;

        private Variable safeGlobalVariable() {
            Variable variable = new Variable();
            setVariableName(variable);
            setVariableType(variable);
            return variable;
        }

        private Variable safeVariable(List<Function> functions) {
            Variable variable = new Variable();
            setVariableType(variable);
            setVariableName(variable);

            if (getInputTokens().size() > 0) {
                Node node = (Node) getInputTokens().get(0);
                checkForTypeMiss(node, variable, functions);
            }
            return variable;
        }

        private void checkForTypeMiss(Node node, Variable variable, List<Function> functions) {
            if (node.getRule().getNonterminal().equals("RESERVED_INTEGER_LITERAL") && !variable.getType().equals("int")) {
                System.out.println("Error: Variable type missmatch." + variable.getName() + getRanges());
                return;
            } else if (node.getRule().getNonterminal().equals("RESERVED_STRING_LITERAL") && !variable.getType().equals("str")) {
                System.out.println("Error: Variable type missmatch." + variable.getName() + getRanges());
                return;
            } else if (node.getRule().getNonterminal().equals("method_invocation")) {
                String functionName = checkIsDeclaredFunction(functions, (Node) node.getInputTokens().get(0));
                for (Function function : functions) {
                    String functionType = function.getType();
                    String type = variable.getType();
                    if (function.getName().equals(functionName) && !functionType.equals(type)) {
                        System.out.println("Error: Variable type missmatch." + variable.getName() + getRanges());
                        break;
                    }
                }
                return;
            } else {
                for (int i = 0; i < getInputTokens().size(); i++) {
                    Object o = getInputTokens().get(i);

                    if (o instanceof Node) {
                        ((Node) o).checkForTypeMiss((Node) o, variable, functions);
                    }
                }
            }
        }

        private void setVariableName(Variable variable) {
            Node node = (Node) getInputTokens().get(1);
            for (int i = 0; i < 2; i++) {
                node = (Node) node.getInputTokens().get(0);
            }
            variable.setName(node.getInputTokens().toString());
        }

        private void setVariableType(Variable variable) {
            Node node = (Node) getInputTokens().get(0);
            Node node1 = (Node) node.getInputTokens().get(0);
            if (node1.getRule().getNonterminal().equals("RESERVED_INT_PRIMITIVE")) {
                variable.setType("int");
            } else if (node1.getRule().getNonterminal().equals("RESERVED_BOOL_PRIMITIVE")) {
                variable.setType("bol");
            } else {
                variable.setType("str");
            }
        }

        private void analyzeBlock(int indent, List<String> funcId, List<String> identifiers, List<Variable> vars, List<Variable> globalVariables, List<Function> functions) {
            Object o = getInputTokens().get(1);
            if (o instanceof Node) {
                ((Node) o).analyzeSemantics(indent + 1, funcId, identifiers, vars, globalVariables, functions);
            }
        }

        private void analyzeIdentifiers(List<String> identifiers, List<String> funcId) {
            Node node = (Node) getInputTokens().get(1);
            for (int i = 0; i < 2; i++) {
                node = (Node) node.getInputTokens().get(0);
            }

            if (node.getRule().getSymbols().contains("`identifier`")) {
                String identifier = node.getInputTokens().get(0).toString();
                if (identifiers.contains(identifier) || funcId.contains(identifier)) {
                    System.out.println("Error: already used identifier:" + identifier + "(" + getRanges() + ")");
                } else {
                    identifiers.add((String) identifier);
                }
            }
        }

        public String getFunctionName() {
            Node node = (Node) inputTokens.get(2);
            return (String) node.getInputTokens().get(0);
        }

        public String getFunctionType() {
            Node node = (Node) inputTokens.get(1);
            if (node.getRule().getSymbols().contains("RESERVED_INT_PRIMITIVE")) {
                return "int";
            } else if (node.getRule().getSymbols().contains("RESERVED_STRING_PRIMITIVE")) {
                return "str";
            } else if (node.getRule().getSymbols().contains("RESERVED_BOOL_PRIMITIVE")) {
                return "bol";
            }
            return "null";
        }

        public List<semantic.Parameter> getFunctionParameters() {
            List<semantic.Parameter> parameters = Lists.newArrayList();
            Node node = (Node) inputTokens.get(4);
            while (node.getRule().getSymbols().contains("RESERVED_COMMA")) {
                parameters.add(saveParameter((Node) node.getInputTokens().get(2)));
                node = (Node) node.getInputTokens().get(0);
            }
            node = (Node) node.getInputTokens().get(0);
            parameters.add(saveParameter(node));
            return parameters;
        }

        private semantic.Parameter saveParameter(Node node) {
            semantic.Parameter parameter = new semantic.Parameter();
            parameter.setType(getParameterType(node));
            parameter.setName(getParameterName(node));
            return parameter;
        }

        private String getParameterName(Node node) {
            node = (Node) node.getInputTokens().get(1);
            node = (Node) node.getInputTokens().get(0);
            return (String) node.getInputTokens().get(0);
        }

        private String getParameterType(Node node) {
            for (int i = 0; i < 3; i++) {
                node = (Node) node.getInputTokens().get(0);
            }
            if (node.getRule().getNonterminal().equals("RESERVED_INT_PRIMITIVE")) {
                return "int";
            } else if (node.getRule().getNonterminal().equals("RESERVED_STRING_PRIMITIVE")) {
                return "str";
            }
            return "bol";
        }

        public String toString(int indent) {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < indent; i++)
                sb.append("  ");

            sb.append(getRule().getNonterminal());
            sb.append(" ::= ");

            if (getRule().rightSize() <= 0)
                sb.append("");
            else
                for (int i = 0; i < getRule().rightSize(); i++)
                    sb.append(getRule().getRightSymbol(i) + " ");

            sb.append("\t=>\t");
            sb.append(getInputTokens());
            sb.append("\n");

            for (int i = 0; i < getInputTokens().size(); i++) {
                Object o = getInputTokens().get(i);

                if (o instanceof Node) {
                    sb.append(((Node) o).toString(indent + 1));
                }
            }

            return sb.toString();
        }


    }
}
