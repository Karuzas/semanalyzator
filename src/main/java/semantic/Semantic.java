package semantic;

import parser.Token;
import parser.syntax.Rule;

import java.util.List;


public interface Semantic {

    Object doSemantic(Rule rule, List<Object> parseResults, List<Token.Range> resultRanges);

}
