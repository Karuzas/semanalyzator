package semantic;

import com.google.common.base.Throwables;
import com.google.common.collect.Lists;
import parser.Lexer;
import parser.Parser;
import parser.parsertables.LRParserTables;
import parser.syntax.builder.SyntaxBuilder;

import java.io.File;
import java.io.FileReader;
import java.util.List;

import static semantic.TreeBuilderSemantic.*;

public class SemanticAnalyzator {

    public static List<String> identifiers = Lists.newArrayList();
    public static List<String> functionIdentifiers = Lists.newArrayList();
    public static List<Variable> variables = Lists.newArrayList();
    public static List<Variable> globalVariables = Lists.newArrayList();
    public static List<Function> functions = Lists.newArrayList();

    public static void main(String[] args) {
        try {
            File syntaxInput = new File(SemanticAnalyzator.class.getResource("/pirmaTM.syntax").toURI());
            SyntaxBuilder builder = new SyntaxBuilder(syntaxInput);
            Parser parser = new Parser(new LRParserTables(builder.getParserSyntax()));
            Lexer lexer = builder.getLexer();
            parser.setLexer(lexer);
            lexer.setDebug(true);

            String fileToParse = "/home/andriusk/IdeaProjects/SemAnalyzator/src/main/java/parser/examples/exampleProgram";
            FileReader parseInput = new FileReader(fileToParse);
            parser.setInput(parseInput);

            parser.parse(lexer, new TreeBuilderSemantic());
            Node node = (Node) parser.getResult();

            node.analyzeSemantics(0, functionIdentifiers, identifiers, variables, globalVariables, functions);

        } catch (Exception e) {
            Throwables.propagate(e);
        }
    }

}
